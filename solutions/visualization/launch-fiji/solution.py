from album.runner.api import setup


def unzip_archive(zip_filename, dest_dir):
    import zipfile
    with zipfile.ZipFile(zip_filename, 'r') as zip_ref:
        zip_ref.extractall(dest_dir)


def make_executable(path):
    import os
    mode = os.stat(path).st_mode
    mode |= (mode & 0o444) >> 2    # copy R bits to X
    os.chmod(path, mode)


def install():
    from album.runner.api import get_app_path, download_if_not_exists
    import platform

    download_link_linux = "https://downloads.imagej.net/fiji/archive/20230711-1417/fiji-linux64.zip"
    download_link_macos = "https://downloads.imagej.net/fiji/archive/20230711-1417/fiji-macosx.zip"
    download_link_windows = "https://downloads.imagej.net/fiji/archive/20230711-1417/fiji-win64.zip"
    download_link = ""
    if platform.system() == 'Windows':
        download_link = download_link_windows
    elif platform.system() == 'Linux':
        download_link = download_link_linux
    elif platform.system() == 'Darwin':
        download_link = download_link_macos

    get_app_path().mkdir(exist_ok=True, parents=True)

    fiji_zip = download_if_not_exists(download_link, "fiji.zip")
    unzip_archive(fiji_zip, get_app_path())
    make_executable(get_fiji_executable())


def run():
    import subprocess
    from album.runner.api import get_args
    from pathlib import Path
    input_path = get_args().input
    params = [get_fiji_executable()]
    if input_path:
        input_path = Path(input_path).absolute()
        params.append(input_path)
    subprocess.run(params, check=True)


def get_fiji_executable():
    import platform
    from album.runner.api import get_app_path
    run_path = []
    run_path_linux = ["Fiji.app", "ImageJ-linux64"]
    run_path_windows = ["Fiji.app", "ImageJ-win64.exe"]
    run_path_macos = ["Fiji.app", "Contents", "MacOS", "ImageJ-macosx"]
    if platform.system() == 'Windows':
        run_path = run_path_windows
    elif platform.system() == 'Linux':
        run_path = run_path_linux
    elif platform.system() == 'Darwin':
        run_path = run_path_macos
    return get_app_path().joinpath(*run_path)


setup(
    group="visualization",
    name="launch-fiji",
    version="0.1.0",
    solution_creators=["Deborah Schmidt"],
    title="Display a dataset in Fiji v20230711-1417",
    description="This solution downloads Fiji in the specified version and launches it. An optional parameter for directly opening a dataset is provided.",
    cite=[{
        "text": "Schindelin, J., Arganda-Carreras, I., Frise, E., Kaynig, V., Longair, M., Pietzsch, T., … Cardona, A. (2012). Fiji: an open-source platform for biological-image analysis. Nature Methods, 9(7), 676–682. ",
        "doi": "doi:10.1038/nmeth.2019",
        "url": "https://fiji.sc"
    }],
    covers=[{
        "description": "The Fiji user interface in front of a generative image displayed in Fiji.",
        "source": "cover.jpg"
    }],
    tags=["visualization", "fiji"],
    album_api_version="0.5.5",
    args=[{
            "name": "input",
            "type": "file",
            "required": False,
            "description": "The image to display in Fiji"
        }],
    install=install,
    run=run,
    dependencies={"environment_file": """channels:
  - conda-forge
  - defaults
dependencies:
  - python=3.9
  - openjdk=11.0.9.1
"""}
)

