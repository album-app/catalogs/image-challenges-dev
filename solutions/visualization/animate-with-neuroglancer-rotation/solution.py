from album.runner.api import setup

# Define the environment file with necessary dependencies
env_file = """
channels:
  - conda-forge
dependencies:
  - python=3.12
  - pip
  - pip:
      - neuroglancer==2.39
      - selenium==4.25.0
"""


def create_viewer(resolution):
    from neuroglancer import webdriver as webd
    import neuroglancer

    viewer = neuroglancer.Viewer()

    webdriver = webd.Webdriver(viewer, headless=True, docker=True, )
    webdriver.driver.set_window_size(resolution[0], resolution[1])

    return viewer


def run():
    from album.runner.api import get_args
    import math
    import os
    import json
    from neuroglancer import server

    # Get arguments
    args = get_args()
    initial_state_file = args.initial_state_json
    static_content_source = args.neuroglancer_source
    bind_address = args.neuroglancer_bind_address
    bind_port = args.neuroglancer_bind_port
    static_content_dev_server = args.neuroglancer_run_server
    no_webdriver = args.no_webdriver
    num_frames = args.num_frames
    resolution = args.resolution.split(",")
    resolution[0] = int(resolution[0])
    resolution[1] = int(resolution[1])
    output_dir = args.output_dir
    position = args.position.split(",")
    vertical_axis = args.vertical_axis

    # Load the initial viewer state from the file
    with open(initial_state_file, 'r') as f:
        initial_state = json.load(f)

    server.debug = True

    if bind_address is not None and bind_port is not None:
        server.set_server_bind_address(bind_address, bind_port)

    if static_content_dev_server:
        server.set_dev_server_content_source()

    if static_content_source:
        server.set_static_content_source(url=static_content_source)

    viewer = create_viewer(resolution)
    print(viewer.get_viewer_url())

    # Define functions
    def quaternion_multiply(q1, q2):
        x1, y1, z1, w1 = q1
        x2, y2, z2, w2 = q2
        x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2
        y = w1 * y2 - x1 * z2 + y1 * w2 + z1 * x2
        z = w1 * z2 + x1 * y2 - y1 * x2 + z1 * w2
        w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2
        return [x, y, z, w]

    def generate_rotation(
        viewer,
        num_frames=360,
        resolution=(960, 540),
        output_dir="output",
        position=None,
        vertical_axis='z',
        initial_state=None
    ):
        # Set the viewer state directly from the provided initial state
        viewer.set_state(initial_state)

        # Hide UI controls for cleaner screenshots
        with viewer.config_state.txn() as s:
            s.show_ui_controls = False
            s.show_panel_borders = False

        counter = 0

        # Determine rotation axis
        axis_map = {'x': [1, 0, 0], 'y': [0, 1, 0], 'z': [0, 0, 1]}
        if vertical_axis not in axis_map:
            raise ValueError("vertical_axis must be 'x', 'y', or 'z'")
        axis_vector = axis_map[vertical_axis]

        # Get the initial orientation from the state
        initial_orientation = initial_state.get('projectionOrientation', [0, 0, 0, 1])

        # Create output directory
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        for i in range(num_frames):
            screenshot_filename = os.path.join(output_dir, f'screenshot_{str(counter).zfill(5)}.png')
            print(f"Generating {screenshot_filename}")

            # Calculate rotation angle
            angle = i * 2 * math.pi / num_frames  # Full rotation over num_frames
            half_angle = angle / 2.0
            sin_half_angle = math.sin(half_angle)
            cos_half_angle = math.cos(half_angle)

            # Rotation quaternion around the specified vertical axis
            rotation_quaternion = [
                axis_vector[0] * sin_half_angle,
                axis_vector[1] * sin_half_angle,
                axis_vector[2] * sin_half_angle,
                cos_half_angle
            ]

            # Combine rotations: new_orientation = rotation * initial_orientation
            new_orientation = quaternion_multiply(rotation_quaternion, initial_orientation)

            # Normalize the quaternion to avoid accumulating errors
            norm = math.sqrt(sum(n * n for n in new_orientation))
            new_orientation = [n / norm for n in new_orientation]

            with viewer.txn() as s:
                s.projectionOrientation = new_orientation
                if position:
                    s.position = position

            # Take a screenshot
            screenshot = viewer.screenshot(resolution)
            with open(screenshot_filename, "wb") as f:
                f.write(screenshot.screenshot.image)
            counter += 1

    # Generate the rotation
    generate_rotation(
        viewer,
        num_frames=num_frames,
        resolution=resolution,
        output_dir=output_dir,
        position=position,
        vertical_axis=vertical_axis,
        initial_state=initial_state
    )


setup(
    group="visualization",
    name="animate-with-neuroglancer-rotation",
    version="0.2.0-SNAPSHOT",
    album_api_version="0.5.5",
    title="Neuroglancer Rotation Animation",
    description="An album solution to generate a rotation animation for a Neuroglancer viewer state provided as JSON.",
    solution_creators=["Deborah Schmidt"],
    tags=["neuroglancer", "animation", "3D"],
    license="MIT",
    covers=[],
    args=[
        {
            "name": "initial_state_json",
            "type": "file",
            "required": True,
            "description": "Initial viewer state in JSON format."
        },
        {
            "name": "neuroglancer_source",
            "type": "string",
            "description": "Source of static content for Neuroglancer rendering, i.e. custom Neuroglancer URL."
        },
        {
            "name": "neuroglancer_bind_address",
            "type": "string",
            "description": "Neuroglancer address"
        },
        {
            "name": "neuroglancer_bind_port",
            "type": "string",
            "description": "Neuroglancer port"
        },
        {
            "name": "neuroglancer_run_server",
            "type": "boolean",
            "description": "Run Neuroglancer from source"
        },
        {
            "name": "num_frames",
            "type": "integer",
            "default": 360,
            "description": "Number of frames in the rotation animation."
        },
        {
            "name": "resolution",
            "type": "string",
            "default": "960,540",
            "description": "Resolution of the output images (width, height)."
        },
        {
            "name": "output_dir",
            "type": "string",
            "default": "output",
            "description": "Directory where screenshots will be saved."
        },
        {
            "name": "position",
            "type": "string",
            "default": "0,0,0",
            "description": "Camera position as [x, y, z]."
        },
        {
            "name": "vertical_axis",
            "type": "string",
            "default": "z",
            "description": "Axis around which to rotate (x, y, or z)."
        }
    ],
    run=run,
    dependencies={'environment_file': env_file}
)
