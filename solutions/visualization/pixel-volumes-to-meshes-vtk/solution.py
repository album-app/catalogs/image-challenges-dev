from album.runner.api import setup

# Please import additional modules at the beginning of your method declarations.
# More information: https://docs.album.solutions/en/latest/solution-development/

def run():
    from album.runner.api import get_args
    from pathlib import Path
    import vtk
    import numpy as np
    import tifffile as tiff  # To read TIFF files
    import os
    from os import listdir
    from os.path import isfile, join

    smoothing_iterations = get_args().smoothing_iterations
    smoothing_pass_band = get_args().smoothing_pass_band
    smoothing_feature_angle = get_args().smoothing_feature_angle
    decimate_percentage = get_args().decimate_percentage
    no_labelmap = not get_args().detect_labelmap
    show_plot = get_args().show_meshes
    exclude_zero = get_args().exclude_zero
    contour_value = get_args().contour_value
    all_meshes = []  # List to hold all the meshes

    def generate_meshes(tiff_path, output_folder, mesh_name=None, no_labelmap=False):
        # Read the TIFF file into a NumPy array
        print("Processing file", tiff_path, "..")
        tiff_data = tiff.imread(tiff_path)  # Keep it generic, let dtype be what it is
        min_data = tiff_data.min()
        max_data = tiff_data.max()
        if contour_value:
            min_data = contour_value
            max_data = contour_value
        # Check if no_labelmap is True
        if no_labelmap:
            print("Processing as continuous volume...")
            process_volume(mesh_name, output_folder, tiff_data, minContour=min_data, maxContour=max_data)

        # If the datatype is integer, process it as labelmap or binary mask
        elif np.issubdtype(tiff_data.dtype, np.integer):
            print("Processing as labelmap or binary mask...")
            unique_region_ids = np.unique(tiff_data)
            print("Generating meshes for regions between ID", np.min(unique_region_ids), "and ID",
                  np.max(unique_region_ids))

            for i, region_id in enumerate(unique_region_ids):

                if region_id == 0 and exclude_zero:
                    continue

                if i % 10 == 0:
                    print("%s/%s Processing regions.." % (i, len(unique_region_ids)))

                # Create a binary mask for the current region ID
                binary_mask = np.where(tiff_data == region_id, 1, 0).astype(np.uint8)
                process_volume(mesh_name, output_folder, binary_mask, region_id)

        # If the datatype is floating, process it as continuous volume
        elif np.issubdtype(tiff_data.dtype, np.floating):
            print("Processing as probability map or continuous volume...")
            process_volume(mesh_name, output_folder, tiff_data, minContour=min_data, maxContour=max_data)

        else:
            raise ValueError(f"Unsupported data type: {tiff_data.dtype}")

    def process_volume(mesh_name, output_folder, data, region_id=None, minContour=1, maxContour=1):
        from vtk.util import numpy_support

        data = np.pad(data, ((1, 1), (1, 1), (1, 1)), 'constant', constant_values=(0,))

        imageData = vtk.vtkImageData()
        imageData.SetDimensions(data.shape)
        imageData.SetSpacing((1, 1, 1))

        # Efficiently converting the whole numpy array to VTK array
        vtk_array = numpy_support.numpy_to_vtk(num_array=data.ravel(), deep=True, array_type=vtk.VTK_UNSIGNED_CHAR)
        imageData.GetPointData().SetScalars(vtk_array)

        discrete = vtk.vtkFlyingEdges3D()
        discrete.SetInputData(imageData)
        # print(data.min(), data.max())
        discrete.GenerateValues(1, minContour, maxContour)
        discrete.Update()

        next_input = discrete.GetOutput()

        if smoothing_iterations > 0:
            smoother = vtk.vtkWindowedSincPolyDataFilter()
            smoother.SetInputData(next_input)
            smoother.SetNumberOfIterations(smoothing_iterations)
            smoother.BoundarySmoothingOn()
            smoother.FeatureEdgeSmoothingOn()
            smoother.SetFeatureAngle(smoothing_feature_angle)
            smoother.SetPassBand(smoothing_pass_band)
            smoother.NonManifoldSmoothingOn()
            smoother.NormalizeCoordinatesOn()
            smoother.Update()
            next_input = smoother.GetOutput()

        if decimate_percentage > 0:
            decimate = vtk.vtkDecimatePro()
            decimate.SetInputData(next_input)
            decimate.SetTargetReduction(decimate_percentage)
            decimate.PreserveTopologyOn()
            decimate.Update()

            next_input = decimate.GetOutput()
            # decimated = vtk.vtkPolyData()
            # decimated.ShallowCopy(decimate.GetOutput())
            # print('Before decimation:')
            # print(f'There are {decimated.GetNumberOfPoints()} points.')
            # print(f'There are {decimated.GetNumberOfPolys()} polygons.')
            # print(
            #     f'Reduction: {(shape.GetNumberOfPolys() - decimated.GetNumberOfPolys()) / shape.GetNumberOfPolys()}')
        # # Compute Normals
        # normals_generator = vtk.vtkPolyDataNormals()
        # normals_generator.SetInputData(next_input)
        # normals_generator.ComputePointNormalsOn()  # Compute Point Normals
        # normals_generator.ComputeCellNormalsOn()  # If needed, Compute Cell Normals too
        # normals_generator.Update()
        #
        # # Replace the next_input with the output from normals_generator
        # next_input = normals_generator.GetOutput()
        # Create PolyData writer
        writer = vtk.vtkSTLWriter()
        if region_id:
            if mesh_name:
                path = f"{output_folder}/{mesh_name}_{region_id}.stl"
            else:
                path = f"{output_folder}/label_{region_id}.stl"
        else:
            if mesh_name:
                path = f"{output_folder}/{mesh_name}.stl"
            else:
                path = f"{output_folder}/mesh.stl"
        writer.SetFileName(path)
        writer.SetInputData(next_input)
        # writer.SetFileTypeToBinary()
        writer.Write()

        if show_plot:
            all_meshes.append(path)

    def is_tiff(file_path):
        import mimetypes
        mime_type, encoding = mimetypes.guess_type(file_path)
        return mime_type == 'image/tiff'


    path = get_args().input
    meshes_path = get_args().output
    colormap = get_args().colormap

    Path(meshes_path).mkdir(parents=True, exist_ok=True)

    if Path(path).is_file():
        file_name_without_extension = os.path.splitext(Path(path).name)[0]
        if is_tiff(path):
            generate_meshes(path, meshes_path, file_name_without_extension, no_labelmap=no_labelmap)
        else:
            print(f"{path} is not a TIFF file.")
    else:
        files = [f for f in listdir(path) if isfile(join(path, f))]
        for i, file in enumerate(files):
            file_path = join(path, file)
            if is_tiff(file_path):
                file_name_without_extension = os.path.splitext(file)[0]
                generate_meshes(file_path, meshes_path, file_name_without_extension, no_labelmap=no_labelmap)
            else:
                print(f"{file} is not a TIFF file.")

    import matplotlib.pyplot as plt
    if show_plot and all_meshes:
        renderer = vtk.vtkRenderer()
        # renderer.SetBackground(1,1,1)  # Setting a background color

        # light = vtk.vtkLight()  # Setting up light
        # light.SetColor(1, 1, 1)  # White color light
        # renderer.AddLight(light)

        for i, mesh_file in enumerate(all_meshes):

            reader = vtk.vtkSTLReader()
            reader.SetFileName(mesh_file)
            reader.Update()

            cmap = plt.get_cmap(colormap)
            color = cmap(float(i) / float(len(all_meshes)))

            mapper = vtk.vtkPolyDataMapper()
            mapper.SetInputConnection(reader.GetOutputPort())

            actor = vtk.vtkActor()
            actor.SetMapper(mapper)
            # actor.GetProperty().SetColor(1, 0, 0)  # Assigning a default red color
            actor.GetProperty().SetColor(color[0], color[1], color[2])  # Assigning the derived color

            renderer.AddActor(actor)

        # light.Modified()
        renderer.Modified()
        renderer.ResetCamera()  # Resetting camera after adding all actors

        renderWindow = vtk.vtkRenderWindow()
        renderWindow.AddRenderer(renderer)

        render_window_interactor = vtk.vtkRenderWindowInteractor()
        render_window_interactor.SetRenderWindow(renderWindow)

        render_window_interactor.Start()


setup(
    group="visualization",
    name="pixel-volumes-to-meshes-vtk",
    version="0.1.0",
    title="Conversion of 3D pixel datasets into meshes with VTK",
    description="This solution uses vtkFlyingEdges3D to generate meshes from pixel volumes, including optional smoothing and decimation of vertices, and can optionally display the result.",
    solution_creators=["Deborah Schmidt"],
    tags=["bioinformatics", "neuroscience", "data-analysis"],
    cite=[{
        "text": "Schroeder, Will; Martin, Ken; Lorensen, Bill (2006), The Visualization Toolkit (4th ed.), Kitware, ISBN 978-1-930934-19-1 ",
        "url": "https://vtk.org"
    }],
    license="MIT",
    album_api_version="0.5.5",
    covers=[{
        "description": "Meshes generated from 3D pixel datasets.",
        "source": "cover.jpg"
    }],
    args=[{
            "name": "input",
            "type": "file",
            "required": True,
            "description": "Path to the 3D TIFF file containing region data or to a folder containing 3D masks."
        }, {
            "name": "output",
            "type": "file",
            "required": False,
            "description": "Path to the resulting collection of meshes."
        }, {
            "name": "detect_labelmap",
            "type": "boolean",
            "default": True,
            "description": "Process int type images as labelmaps."
        }, {
            "name": "contour_value",
            "type": "float",
            "required": False,
            "description": "In case continuous datasets are provided, this value can be set to generate meshes at a specific contour value. Otherwise it will be calculated between min and max of the dataset range."
        }, {
            "name": "smoothing_iterations",
            "type": "integer",
            "default": 2,
            "description": "Smoothing iterations. Set to 0 to skip smoothing."
        }, {
            "name": "smoothing_pass_band",
            "type": "float",
            "default": 0.1,
            "description": "Smoothing pass band."
        }, {
            "name": "smoothing_feature_angle",
            "type": "float",
            "default": 120.,
            "description": "Smoothing feature angle."
        }, {
            "name": "decimate_percentage",
            "type": "float",
            "default": 0.5,
            "description": "How much to decimate vertices after mesh generation (0-1, 0 means do not decimate)."
        }, {
            "name": "show_meshes",
            "type": "boolean",
            "default": False,
            "description": "Whether to display the generated mesh plots or not."
        }, {
            "name": "colormap",
            "type": "string",
            "default": "viridis",
            "description": "Name of the matplotlib colormap to use for coloring meshes."
        }, {
            "name": "exclude_zero",
            "type": "boolean",
            "default": True,
            "description": "Exclude 0 as a label when processing the volumes."
        }
    ],
    run=run,
    dependencies={'parent': {'resolve_solution': 'visualization:visualize-meshes-vtk:0.1.0'}}
)