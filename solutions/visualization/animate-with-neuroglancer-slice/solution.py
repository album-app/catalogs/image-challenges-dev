from album.runner.api import setup

# Define the environment file with necessary dependencies
env_file = """
channels:
  - conda-forge
dependencies:
  - python=3.12
  - pip
  - pip:
      - neuroglancer==2.40.1
      - selenium==4.25.0
"""

def create_viewer(resolution):
    from neuroglancer import webdriver as webd
    import neuroglancer

    viewer = neuroglancer.Viewer()

    webdriver = webd.Webdriver(viewer, headless=True, docker=True)
    webdriver.driver.set_window_size(resolution[0], resolution[1])

    return viewer



def run():
    from album.runner.api import get_args
    import os
    import json

    # Get arguments
    args = get_args()
    initial_state_file = args.initial_state_json
    num_frames = args.num_frames
    resolution = args.resolution.split(",")
    resolution[0] = int(resolution[0])
    resolution[1] = int(resolution[1])
    output_dir = args.output_dir
    position = args.position.split(",")
    vertical_axis = args.vertical_axis

    # Load the initial viewer state from the file
    with open(initial_state_file, 'r') as f:
        initial_state = json.load(f)

    viewer = create_viewer(resolution)
    print(viewer.get_viewer_url())

    def generate_slices(
            viewer,
            num_slices=100,
            resolution=(960, 540),
            output_dir="output",
            position=None,
            slicing_axis='z',
            initial_state=None
    ):
        # Set the viewer state directly from the provided initial state
        viewer.set_state(initial_state)

        # Hide UI controls for cleaner screenshots
        with viewer.config_state.txn() as s:
            s.show_ui_controls = False
            s.show_panel_borders = False

        counter = 0

        # Determine slicing axis
        axis_map = {'x': 0, 'y': 1, 'z': 2}
        if slicing_axis not in axis_map:
            raise ValueError("slicing_axis must be 'x', 'y', or 'z'")
        axis_index = axis_map[slicing_axis]

        # Get the initial position from the state
        initial_position = list(map(float, initial_state.get('position', position)))

        # Get the dataset bounds (this is usually in the viewer state or metadata)
        bounds = [1600,2640,2280]  # Replace this with actual dataset bounds if available
        min_position = 0  # Start at the minimum (e.g., 0)
        max_position = bounds[axis_index]  # Use the dataset's max bound along the slicing axis

        # Calculate the step size based on the number of slices
        step_size = (max_position - min_position) / num_slices

        print(initial_position)

        # Create output directory
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        for i in range(num_slices):
            screenshot_filename = os.path.join(output_dir, f'slice_{str(i).zfill(5)}.png')
            if os.path.exists(screenshot_filename):
                print(f"{screenshot_filename} already exists, skipping.")
                continue
            print(f"Generating {screenshot_filename}")

            # Set position along the slicing axis from min to max
            new_position = initial_position[:]
            new_position[axis_index] = min_position + i * step_size

            with viewer.txn() as s:
                s.position = new_position

            # Take a screenshot
            screenshot = viewer.screenshot(resolution)
            with open(screenshot_filename, "wb") as f:
                f.write(screenshot.screenshot.image)

    generate_slices(
        viewer,
        num_slices=num_frames,  # num_frames now refers to the number of slices
        resolution=resolution,
        output_dir=output_dir,
        position=position,
        slicing_axis=vertical_axis,  # reuse the vertical_axis argument to control slicing
        initial_state=initial_state
    )


setup(
    group="visualization",
    name="animate-with-neuroglancer-slice",
    version="0.1.0-SNAPSHOT",
    album_api_version="0.5.5",
    title="Neuroglancer Slice Animation",
    description="An album solution to generate a rotation animation for a Neuroglancer viewer state provided as JSON.",
    solution_creators=["Deborah Schmidt"],
    tags=["neuroglancer", "animation", "3D"],
    license="MIT",
    covers=[],
    args=[
        {
            "name": "initial_state_json",
            "type": "file",
            "required": True,
            "description": "Initial viewer state in JSON format."
        },
        {
            "name": "num_frames",
            "type": "integer",
            "default": 360,
            "description": "Number of frames in the rotation animation."
        },
        {
            "name": "resolution",
            "type": "string",
            "default": "960,540",
            "description": "Resolution of the output images (width, height)."
        },
        {
            "name": "output_dir",
            "type": "string",
            "default": "output",
            "description": "Directory where screenshots will be saved."
        },
        {
            "name": "position",
            "type": "string",
            "default": "0,0,0",
            "description": "Camera position as [x, y, z]."
        },
        {
            "name": "vertical_axis",
            "type": "string",
            "default": "y",
            "description": "Axis around which to rotate (x, y, or z)."
        }
    ],
    run=run,
    dependencies={'environment_file': env_file}
)
