from album.runner.api import setup

# Please import additional modules at the beginning of your method declarations.
# More information: https://docs.album.solutions/en/latest/solution-development/


def run():
    import vtk

    from album.runner.api import get_args

    args = get_args()
    meshes_folder = args.meshes
    volume = args.volume
    exclude_zero = args.exclude_zero
    include = args.include
    exclude = args.exclude
    colormap = args.colormap
    if include:
        include = include.split(",")
    if exclude:
        exclude = exclude.split(",")
    rendering_size = [int(x) for x in args.size.split("x")]
    output_mp4 = args.output_mp4
    if output_mp4:
        output_mp4.parent.mkdir(parents=True, exist_ok=True)

    renderer = add_meshes_as_actors(meshes_folder, include=include, exclude=exclude, exclude_zero=exclude_zero, colormap=colormap)

    if volume:
        renderer.AddVolume(create_volume_node(volume))

    renderWindow = vtk.vtkRenderWindow()
    renderWindow.AddRenderer(renderer)

    if output_mp4:
        render_video(output_mp4, rendering_size, renderWindow, renderer)
    else:
        render_window_interactor = vtk.vtkRenderWindowInteractor()
        render_window_interactor.SetRenderWindow(renderWindow)
        render_window_interactor.Start()


def create_volume_node(volume):
    from tifffile import imread
    import numpy as np
    import vtk
    from vtk.util import numpy_support

    tiff_data = imread(volume).astype(np.uint16)
    # Calculating min and max values from tiff_data
    min_val = tiff_data.min()
    max_val = tiff_data.max()

    imageData = vtk.vtkImageData()
    imageData.SetDimensions(tiff_data.shape)
    imageData.SetSpacing((1, 1, 1))

    # Efficiently converting the whole numpy array to VTK array
    vtk_array = numpy_support.numpy_to_vtk(num_array=tiff_data.ravel(), deep=True, array_type=vtk.VTK_UNSIGNED_CHAR)
    imageData.GetPointData().SetScalars(vtk_array)

    # Setting up color and opacity transfer functions
    colorTransferFunction = vtk.vtkColorTransferFunction()
    colorTransferFunction.AddRGBPoint(min_val, 0, 0, 0)  # Mapping min_val to black
    colorTransferFunction.AddRGBPoint(max_val, 1, 1, 1)  # Mapping max_val to white

    opacityTransferFunction = vtk.vtkPiecewiseFunction()
    opacityTransferFunction.AddPoint(min_val, 0.0)  # Fully transparent at min_val
    opacityTransferFunction.AddPoint(max_val, 0.5)  # Semi-transparent at max_val

    volumeProperty = vtk.vtkVolumeProperty()
    volumeProperty.SetColor(colorTransferFunction)
    volumeProperty.SetScalarOpacity(opacityTransferFunction)

    volume = vtk.vtkVolume()
    volumeMapper = vtk.vtkSmartVolumeMapper()
    volumeMapper.SetInputData(imageData)
    volume.SetProperty(volumeProperty)
    volume.SetMapper(volumeMapper)
    return volume


def add_meshes_as_actors(meshes_folder, include=None, exclude=None, exclude_zero=True, colormap="rainbow_r"):
    import vtk
    import matplotlib.pyplot as plt
    import matplotlib.colors as mcolors  # For custom colormap
    import glob
    import re
    # Choose colormap
    cmap = plt.get_cmap(colormap)
    # Create a lookup table for coloring
    # lut = vtk.vtkLookupTable()
    # lut.SetNumberOfTableValues(256)
    # lut.Build()
    # Initialize the colormap and normalizer
    renderer = vtk.vtkRenderer()

    files = glob.glob(f"{meshes_folder}/*.stl")

    norm = mcolors.Normalize(vmin=0, vmax=len(files))

    pattern = re.compile(r'([^/]+?)(?:_(\d+))?\..*$')
    for i, vtk_file in enumerate(files):

        if include:
            if not any(_include in str(vtk_file) for _include in include):
                continue
        if exclude:
            if any(_exclude in str(vtk_file) for _exclude in exclude):
                continue
        match = pattern.search(vtk_file)
        region_id = None
        if match:
            base_name, region_id = match.groups()
        if region_id and int(region_id) == 0 and exclude_zero:
            continue
        # Load the VTK file
        reader = vtk.vtkSTLReader()
        reader.SetFileName(vtk_file)
        reader.Update()
        # Create mapper and actor
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputConnection(reader.GetOutputPort())

        actor = vtk.vtkActor()
        actor.SetMapper(mapper)

        color_rgba = cmap(norm(i))
        actor.GetProperty().SetColor(color_rgba[:3])

        renderer.AddActor(actor)
    return renderer


def render_video(output_mp4, rendering_size, renderWindow, renderer):
    import vtk
    import cv2
    from vtk.util.numpy_support import vtk_to_numpy
    import numpy as np


    renderer.ResetCamera()
    renderer.GetActiveCamera().Azimuth(-90)
    renderer.ResetCamera()
    renderer.GetActiveCamera().Elevation(-90)
    renderer.GetActiveCamera().OrthogonalizeViewUp()
    renderer.ResetCamera()
    renderer.GetActiveCamera().Azimuth(90)
    renderer.ResetCamera()

    renderWindow.SetSize(rendering_size[0], rendering_size[1])
    renderWindow.SetOffScreenRendering(1)  # Enable offscreen rendering
    # Create a window-to-image filter
    w2if = vtk.vtkWindowToImageFilter()
    w2if.SetInput(renderWindow)
    w2if.Update()
    # Initialize OpenCV VideoWriter
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(str(output_mp4), fourcc, 20.0, (rendering_size[0], rendering_size[1]))
    renderWindow.Render()  # Create a window-to-image filter
    w2if = vtk.vtkWindowToImageFilter()
    w2if.SetInput(renderWindow)
    w2if.Update()

    # Initialize OpenCV VideoWriter
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(str(output_mp4), fourcc, 20.0, (rendering_size[0], rendering_size[1]))

    duration_rotation1 = 180
    angle_rotation1 = 360
    duration_rotation2 = 180
    angle_rotation2 = 360
    duration_rotation3 = 30
    angle_rotation3 = -90
    duration_zoom = 360
    depth_zoom = 800
    clip_distance = 150
    num_frames = duration_rotation1+duration_rotation2+duration_rotation3+duration_zoom
    # Initialize a clipping plane
    clipping_plane = vtk.vtkPlane()
    clipping_plane.SetNormal(0, 1, 0)  # set the normal vector of the plane
    clipping_plane.SetOrigin(0, -clip_distance, 0)  # set a point the plane goes through

    # Create a Plane Collection and add the clipping plane to it
    actor_clipper_dict = create_clippers_for_actors(clipping_plane, renderer)

    # Rotate and capture frames
    for i in range(num_frames):
        # Rotate camera along Azimuth for the first half
        if i < duration_rotation1:
            renderer.GetActiveCamera().Azimuth(float(angle_rotation1)/float(duration_rotation1))
            renderer.ResetCamera()
        # Rotate camera along Elevation for the second half
        else:
            if i < duration_rotation1+duration_rotation2:
                renderer.GetActiveCamera().Elevation(float(angle_rotation2)/float(duration_rotation2))
                renderer.GetActiveCamera().OrthogonalizeViewUp()
                renderer.ResetCamera()
            else:
                if i < duration_rotation1+duration_rotation2+duration_rotation3:
                    renderer.GetActiveCamera().Azimuth(float(angle_rotation3)/float(duration_rotation3))
                    renderer.ResetCamera()
                else:
                    new_origin_x = (i-duration_rotation1-duration_rotation2-duration_rotation3) * float(depth_zoom)/float(duration_zoom)
                    clipping_plane.SetOrigin(0, new_origin_x-clip_distance, 0)
                    camera_x, camera_y, camera_z = renderer.GetActiveCamera().GetPosition()
                    renderer.GetActiveCamera().SetPosition(camera_x, camera_y+float(depth_zoom)/float(duration_zoom), camera_z)

                    # Update each actor's clipped data
                    for actor in renderer.GetActors():
                        clipper = actor_clipper_dict[actor]
                        clipper.Update()

        renderWindow.Render()

        # Capture frame from VTK and convert to OpenCV format
        w2if.Modified()
        w2if.Update()
        vtk_image = w2if.GetOutput()

        # Convert vtk_image to an OpenCV image (assuming 3 components per pixel, uint8)
        width, height, _ = vtk_image.GetDimensions()
        components = vtk_image.GetNumberOfScalarComponents()

        temp_array = vtk_to_numpy(vtk_image.GetPointData().GetScalars())
        temp_array = temp_array.reshape(height, width, components)
        temp_array = np.flipud(temp_array)

        out.write(temp_array)

        if i % 10 == 0:
            print(f"Writing frame {i + 1}..")
    # Release resources
    out.release()


def create_clippers_for_actors(clipping_plane, renderer):
    import vtk
    clipPlanes = vtk.vtkPlaneCollection()
    clipPlanes.AddItem(clipping_plane)
    actor_clipper_dict = {}
    for actor in renderer.GetActors():
        clipper = vtk.vtkClipClosedSurface()
        poly_data = actor.GetMapper().GetInput()
        clipper.SetInputData(poly_data)
        clipper.SetClippingPlanes(clipPlanes)
        clipper.Update()
        actor.GetMapper().SetInputData(clipper.GetOutput())
        actor_clipper_dict[actor] = clipper
    return actor_clipper_dict


setup(
    group="visualization",
    name="visualize-meshes-vtk",
    version="0.1.0",
    title="Visualize meshes in VTK",
    description="Displays meshes and optionally renders them to video.",
    solution_creators=["Deborah Schmidt"],
    tags=[],
    cite=[{
        "text": "Schroeder, Will; Martin, Ken; Lorensen, Bill (2006), The Visualization Toolkit (4th ed.), Kitware, ISBN 978-1-930934-19-1 ",
        "url": "https://vtk.org"
    }],
    covers=[{
        "description": "Meshes from a generic dataset visualized with VTK.",
        "source": "cover.jpg"
    }],
    license="MIT",
    album_api_version="0.5.5",
    args=[{
            "name": "meshes",
            "type": "directory",
            "required": True,
            "description": "Path to the meshes."
        }, {
            "name": "volume",
            "type": "file",
            "required": False,
            "description": "Optional volume to render next to the meshes."
        }, {
            "name": "include",
            "type": "string",
            "description": "List of names of elements which should be loaded, comma separated",
            "required": False
        }, {
            "name": "exclude",
            "type": "string",
            "description": "List of names of elements which should not be loaded, comma separated",
            "required": False
        }, {
            "name": "colormap",
            "type": "string",
            "default": "viridis",
            "description": "Name of the matplotlib colormap to use for coloring meshes."
        }, {
            "name": "output_mp4",
            "type": "file",
            "required": False,
            "description": "If provided, the result is not visualized in a window, but rendered to this path."
        },
        {
            "name": "size",
            "type": "string",
            "default": "1600x900",
            "description": "Size of the video in WxH (i.e. \"800x600\")"
        }, {
            "name": "exclude_zero",
            "type": "boolean",
            "default": True,
            "description": "Exclude 0 as a label when processing the volumes."
        }
    ],
    run=run,
    dependencies={'environment_file': """channels:
  - conda-forge
  - defaults
dependencies:
  - python=3.9
  - pandas=1.5.3
  - matplotlib-base=3.7.0
  - numpy=1.24.2
  - vtk=9.2.6
  - numpy-stl=3.0.1
  - opencv=4.8.0
  - pip
  - pip:
    - vtkplotlib==2.1.0
    - imagecodecs
    - tifffile==2023.9.26
"""}
)