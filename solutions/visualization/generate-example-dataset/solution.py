from album.runner.api import setup

def normalize_channel(channel):
    import numpy as np
    channel_min = channel.min()
    channel_max = channel.max()
    return ((channel - channel_min) / (channel_max - channel_min) * 255.).astype(np.float32)


def export(out_dir, output1, output2, output3):
    from pathlib import Path
    import tifffile as tiff
    import numpy as np
    from scipy import ndimage
    import pandas as pd
    from skimage import measure

    # output = np.multiply(output, 255).astype(np.uint8)
    # base_output_path = get_cache_path()
    base_output_path = Path(out_dir)
    print("Exporting to %s.." % str(base_output_path))

    channel1 = normalize_channel(output1)
    channel2 = normalize_channel(output2)
    channel3 = normalize_channel(output3)

    labeled_image, num_features = ndimage.label(channel3 > 10)
    tiff.imwrite(base_output_path.joinpath("raw_channel1.tif"), channel1, compression ='zlib')
    tiff.imwrite(base_output_path.joinpath("raw_channel2.tif"), channel2, compression ='zlib')
    tiff.imwrite(base_output_path.joinpath("raw_channel3.tif"), channel3, compression ='zlib')
    tiff.imwrite(base_output_path.joinpath("segmentation_channel1.tif"), ((output1 > 40)*255).astype(np.uint8), compression ='zlib')
    tiff.imwrite(base_output_path.joinpath("segmentation_channel2.tif"), ((output2 > 252)*255).astype(np.uint8), compression ='zlib')
    tiff.imwrite(base_output_path.joinpath("labelmap_channel3.tif"), labeled_image, compression ='zlib')

    # Compute properties for labeled_image
    properties = measure.regionprops_table(labeled_image, properties=('label', 'area', 'major_axis_length', 'minor_axis_length'))

    # Convert to DataFrame for easy CSV writing
    df = pd.DataFrame(properties)

    # Compute Elongation
    df['elongation'] = df['major_axis_length'] / df['minor_axis_length']

    # Write to CSV
    df.to_csv(base_output_path.joinpath('analysis_data_channel3.csv'), index=False)



def randrange(min, max):
    from random import randrange
    if min < max:
        return randrange(int(min), int(max))
    else:
        return randrange(int(max), int(min))


def get_prog_key(prog, key):
    try:
        return prog[key]
    except KeyError:
        return None


def run():
    import moderngl
    import numpy as np
    from PIL import Image, ImageFilter
    from album.runner.api import get_cache_path, get_args

    ctx = moderngl.create_standalone_context()
    prog = ctx.program(
        vertex_shader='''
        #version 330
        in vec2 in_vert;
        out vec2 v_text;
        void main() {
            gl_Position = vec4(in_vert, 0.0, 1.0);
            v_text = in_vert;
        }
            ''',
        fragment_shader='''
#version 330
in vec2 v_text;
out vec4 f_color;
uniform float depth;
uniform float max_width;
uniform float max_height;
uniform float max_depth;
uniform vec3 c1;
uniform vec3 c2;
uniform vec3 c3;
uniform float diff;
const float PI = 3.14159265359;

float rand(vec2 co){
    return fract(sin(dot(co, vec2(12.9898, 78.233))) * 43758.5453);
}

//	Simplex 3D Noise 
//	by Ian McEwan, Ashima Arts
//
vec4 permute(vec4 x){return mod(((x*34.0)+1.0)*x, 289.0);}
vec4 taylorInvSqrt(vec4 r){return 1.79284291400159 - 0.85373472095314 * r;}

float snoise(vec3 v){ 
  const vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
  const vec4  D = vec4(0.0, 0.5, 1.0, 2.0);

// First corner
  vec3 i  = floor(v + dot(v, C.yyy) );
  vec3 x0 =   v - i + dot(i, C.xxx) ;

// Other corners
  vec3 g = step(x0.yzx, x0.xyz);
  vec3 l = 1.0 - g;
  vec3 i1 = min( g.xyz, l.zxy );
  vec3 i2 = max( g.xyz, l.zxy );

  //  x0 = x0 - 0. + 0.0 * C 
  vec3 x1 = x0 - i1 + 1.0 * C.xxx;
  vec3 x2 = x0 - i2 + 2.0 * C.xxx;
  vec3 x3 = x0 - 1. + 3.0 * C.xxx;

// Permutations
  i = mod(i, 289.0 ); 
  vec4 p = permute( permute( permute( 
             i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
           + i.y + vec4(0.0, i1.y, i2.y, 1.0 )) 
           + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));

// Gradients
// ( N*N points uniformly over a square, mapped onto an octahedron.)
  float n_ = 1.0/7.0; // N=7
  vec3  ns = n_ * D.wyz - D.xzx;

  vec4 j = p - 49.0 * floor(p * ns.z *ns.z);  //  mod(p,N*N)

  vec4 x_ = floor(j * ns.z);
  vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)

  vec4 x = x_ *ns.x + ns.yyyy;
  vec4 y = y_ *ns.x + ns.yyyy;
  vec4 h = 1.0 - abs(x) - abs(y);

  vec4 b0 = vec4( x.xy, y.xy );
  vec4 b1 = vec4( x.zw, y.zw );

  vec4 s0 = floor(b0)*2.0 + 1.0;
  vec4 s1 = floor(b1)*2.0 + 1.0;
  vec4 sh = -step(h, vec4(0.0));

  vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
  vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;

  vec3 p0 = vec3(a0.xy,h.x);
  vec3 p1 = vec3(a0.zw,h.y);
  vec3 p2 = vec3(a1.xy,h.z);
  vec3 p3 = vec3(a1.zw,h.w);

//Normalise gradients
  vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
  p0 *= norm.x;
  p1 *= norm.y;
  p2 *= norm.z;
  p3 *= norm.w;

// Mix final noise value
  vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
  m = m * m;
  return 42.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1), 
                                dot(p2,x2), dot(p3,x3) ) );
}

float distance(vec3 c1, vec3 localizable) {
    float x = c1[0] - localizable[0];
    float y = c1[1] - localizable[1];
    float z = c1[2] - localizable[2];
    float d = sqrt(x * x + y * y + z * z);
    return d;
}

float calcHull(
    vec3 localizable,
    float radius,
    vec3 c1,
    vec3 c2,
    vec3 c3) {
    float minC1 = radius / distance(c1, localizable);
    float minC2 = radius / distance(c2, localizable);
    float minC3 = radius / distance(c3, localizable);
    float res = (minC1 + minC2 + minC3);
    return res;
}

float calcDeltaHull(
    vec3 localizable,
    vec3 c1,
    vec3 c2,
    vec3 c3,
    int dimension) {
    float x = localizable.x;
    float y = localizable.y;
    float z = localizable.z;
    float minC1 = -(c1[dimension] - localizable[dimension])/sqrt(pow(c1.x - x, 2.0) + pow(c1.y - y, 2.0) + pow(c1.z - z, 2.0));
    float minC2 = -(c2[dimension] - localizable[dimension])/sqrt(pow(c2.x - x, 2.0) + pow(c2.y - y, 2.0) + pow(c2.z - z, 2.0));
    float minC3 = -(c3[dimension] - localizable[dimension])/sqrt(pow(c3.x - x, 2.0) + pow(c3.y - y, 2.0) + pow(c3.z - z, 2.0));
    return minC1 + minC2 + minC3;
}

 vec2 fold(vec2 p, float ang){
    vec2 n=vec2(cos(-ang),sin(-ang));
    p-=2.*min(0.,dot(p,n))*n;
    return p;
}
#define PI 3.14159

const float time = -0.50;
vec2 gen_fold(vec2 pt) {
    pt = fold(pt,-2.9);
    pt = fold(pt,.9);
    pt.y+=sin(time)+1.;
    pt = fold(pt,-1.0);
    return pt;
}
vec2 gen_curve(vec2 pt) {
    for(int i=0;i<9;i++){
        pt*=2.;
        pt.x-=1.;
        pt=gen_fold(pt);
    }
    return pt;
}
float d2hline(vec2 p){
    p.x-=max(0.,min(1.,p.x));
    return length(p)*5.;
}

const float _time = 0.413;
vec3 tri_fold(vec3 pt) {
    pt.xy = fold(pt.xy,0.947);
    pt.yz = fold(pt.yz,-21.338);
    pt.xy = fold(pt.xy,-0.191);
    pt.yz = fold(pt.yz,-3.214/3.164+sin(0.413*0.884)/-0.071);
    return pt;
}
vec3 tri_curve(vec3 pt) {
    for(int i=0;i<10;i++){
        pt*=1.592;
        pt.x-=8.672;
        pt=tri_fold(pt);
    }
    return pt;
}
float DE(vec3 p){
    p *= 3.0643;
    p.x+=2.168;
    p.y-=2.368;
    p.z+=0.268;
    p=tri_curve(p);
    return 5.528*(length( p*0.0028 ) - 0.052);
}

void main() {
    float x = (v_text.x * 0.5 + 0.5) * max_width;
    float y = (v_text.y * 0.5 + 0.5) * max_height;
    float z = (depth * 0.5 + 0.5) * max_depth;
    
    vec3 localizable = vec3(x, y, z); 
    float t = 0.0;
    float f = 5.0 / max_depth;
    float dt = 0.618;
    float res = calcHull(localizable, diff, c1, c2, c3);
    float resClamped = res;
    float maxSum = 1.8;
    float minSum = 1.5;
    float ramp = 3.3;
    if(res > maxSum) {
        resClamped = max(0.0, maxSum + ramp*maxSum - ramp*res);
    } else {
        if(res < minSum) {
            resClamped = max(0.0, minSum + res*ramp-minSum*ramp);
        }
    }
    
    float val1 = 0;
    float val2 = 0;
    float val3 = 0;
    
    float dx = calcDeltaHull(localizable, c1, c2, c3, 0);
    float dy = calcDeltaHull(localizable, c1, c2, c3, 1);
    float dz = calcDeltaHull(localizable, c1, c2, c3, 2);
    float roundFactor = 300.0*res;
    float resDiff = float(int(res*roundFactor)) / roundFactor - res;
    vec3 nextRasterPoint = vec3(x + resDiff/dx, y + resDiff/dy, z + resDiff/dz);
    float randomX = snoise(vec3((x + t * dt) * f, (y + t * dt) * f, (z + t * dt*0.2) * f));
    float randomY = snoise(vec3((x + t * dt*0.34) * f, (y + t * dt) * f*0.94, (z + t * dt) * f*1.04));
    float randomZ = snoise(vec3((x + t * dt*1.2) * f, (y + t * dt) * f, (z + t * dt) * f*0.7));
    nextRasterPoint.x += float(int(randomX * 32));
    nextRasterPoint.y += float(int(randomY * 32));
    nextRasterPoint.z += float(int(randomZ * 32));
    val1 = res*15;
    val2 = resClamped * max(
        0.0,
        sin(x * 0.085 * resClamped) + cos(y * 0.096 * resClamped) + sin(z * 0.074 * resClamped) - 2
    ) * 155;
    resClamped = max(0.0, 20 * resClamped - abs(distance(nextRasterPoint, localizable)))*10;
    resClamped *= max(0.0, sin((resClamped-0.85)*30));
    val3 = max(0.0, 255-255*DE(vec3(x/(max_width), y/(max_height), z/(max_depth))*2.-1.));
    float bounds = 2;
    if(x < bounds || x > max_width-bounds || y < bounds || y > max_height-bounds || z < bounds || z > max_depth-bounds) {
        f_color = vec4(0.0, 0.0, 0.0, 1.0);
    } else {
        f_color = vec4(max(0, min(val1/255., 1)), max(0, min(val2/255., 1)), max(0, min(val3/255., 1)), 1.0);
    }    
}
''')

    size = get_args().size

    depth = get_prog_key(prog, 'depth')
    max_width = get_prog_key(prog, 'max_width')
    max_height = get_prog_key(prog, 'max_height')
    max_depth = get_prog_key(prog, 'max_depth')
    c1 = get_prog_key(prog, 'c1')
    c2 = get_prog_key(prog, 'c2')
    c3 = get_prog_key(prog, 'c3')

    diff = float(size)/40.
    center = float(size)/2.
    _diff = get_prog_key(prog, 'diff')
    if _diff:
        _diff.value = int(float(size)/4)
    if c1:
        c1.value = (randrange(int(center - diff * 2), int(center - diff)),
                    randrange(int(center - diff * 2), int(center - diff)),
                    randrange(center - diff * 2, int(center - diff)))
    if c2:
        c2.value = (randrange(int(center-diff), int(center+diff)),
                    randrange(int(center-diff), int(center+diff)),
                    randrange(int(center-diff), int(center+diff)))
    if c3:
        c3.value = (randrange(center+diff, center+diff*2),
                    randrange(center+diff, center+diff*2),
                    randrange(center+diff, center+diff*2))

    vertices = np.array([-1.0, -1.0, -1.0, 1.0, 1.0, -1.0, 1.0, 1.0])
    w = size
    h = size
    d = size
    if max_width:
        max_width.value = w
    if max_height:
        max_height.value = h
    if max_depth:
        max_depth.value = d

    vbo = ctx.buffer(vertices.astype('f4'))
    vao = ctx.simple_vertex_array(prog, vbo, 'in_vert')
    data1 = np.zeros([w, h, d], dtype=np.float32)
    data2 = np.zeros([w, h, d], dtype=np.float32)
    data3 = np.zeros([w, h, d], dtype=np.float32)

    fbo = ctx.simple_framebuffer((w, h), components=4)
    fbo.use()

    for z in range(0, d):

        fbo.clear(0.0, 0.0, 0.0, 1.0)
        if depth:
            depth.value = (float(z)/float(d))*2 - 1

        vao.render(moderngl.TRIANGLE_STRIP)

        image = Image.frombytes("RGBA", fbo.size, fbo.read(components=4))
        image = image.transpose(Image.FLIP_TOP_BOTTOM)
        image = image.filter(ImageFilter.GaussianBlur(radius=1))
        output = np.array(image)
        image = image.filter(ImageFilter.GaussianBlur(radius=5))
        output_gauss = np.array(image)
        get_cache_path().mkdir(parents=True, exist_ok=True)

        output1 = output[:, :, 0]
        output2 = output_gauss[:, :, 1]
        output3 = output[:, :, 2]

        data1[:, :, z] = output1
        data2[:, :, z] = output2
        data3[:, :, z] = output3

    fbo.release()

    get_cache_path().mkdir(parents=True, exist_ok=True)
    export(get_args().output, data1, data3, data2)


setup(
    group="visualization",
    name="generate-example-dataset",
    version="0.1.0",
    album_api_version="0.5.5",
    title="Example dataset generator for 3D TIFFs",
    description="Dataset generator for testing the visualization solutions. It will generate a 3D TIFF dataset with 3 channels. It will also store the channels and a segmentation of each channel separately as TIFFs. Particle analysis is performed on one channel and stored as a labelmap, a table with including the elongation of each label object is stored as well.",
    solution_creators=['Deborah Schmidt'],
    covers=[{
        "description": "Fiji displaying the files generated by this solution.",
        "source": "cover.jpg"
    }],
    license='MIT',
    run=run,
    cite=[
        {
            'text': 'Szabolcs Dombi; ModernGL, high performance python bindings for OpenGL 3.3+.',
            'url': 'https://github.com/moderngl/moderngl'
        },
        {
            'text': 'Patricio Gonzalez Vivo; GLSL Noise Algorithms.',
            'url': 'https://gist.github.com/patriciogonzalezvivo/670c22f3966e662d2f83'
        },
        {
            'text': 'Roy Wiggins; 3D Kaleidoscopic Fractals: Folding the Koch Snowflake.',
            'url': 'http://roy.red/posts/folding-the-koch-snowflake/'
        }
    ],
    args=[{
        "name": "output",
        "type": "directory",
        "description": "Where resulting TIFF files and the table should be stored.",
        "required": True
    }, {
        "name": "size",
        "type": "integer",
        "description": "The size of the resulting dataset (this value is chosen for width, height, and depth).",
        "default": 512
    }],
    dependencies={'environment_file': """channels:
  - conda-forge
  - defaults
dependencies:
  - python=3.8
  - moderngl=5.6.4
  - numpy=1.21.2
  - scikit-image=0.18.3
  - Pillow=8.4.0
  - numpy-stl=2.16.3
  - pandas
"""}
)
