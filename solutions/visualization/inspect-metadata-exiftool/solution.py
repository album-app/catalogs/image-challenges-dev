from album.runner.api import setup

# Please import additional modules at the beginning of your method declarations.
# More information: https://docs.album.solutions/en/latest/solution-development/

def run():
    import exiftool
    import glob
    from pprint import pprint
    from album.runner.api import get_args
    from pathlib import Path
    import os

    folder_path = Path(get_args().input).absolute()
    file_pattern = get_args().file_pattern
    ignore_keys = get_args().ignore_keys

    if not Path(folder_path).is_dir():
        files = [str(folder_path)]
    else:
        # This will recursively find all files matching the pattern within the specified folder and its subfolders
        files = glob.glob(f"{folder_path}/**/{file_pattern}", recursive=True)

    with exiftool.ExifToolHelper() as et:
        metadata = et.get_metadata(files)

        # Initialize common_metadata with metadata from the first file
        common_metadata = {key: value for key, value in metadata[0].items() if key not in ignore_keys}

        # Iterate over metadata from the remaining files
        for d in metadata[1:]:
            # Get set of keys which are in both common_metadata and the current file’s metadata
            common_keys = common_metadata.keys() & d.keys()

            # Remove keys from common_metadata that are not in the current file’s metadata or have different values
            common_metadata = {key: common_metadata[key] for key in common_keys if common_metadata[key] == d[key]}

            if not common_metadata:  # Break the loop if there are no common keys left
                break

        # Print common parameters
        if common_metadata:
            if Path(folder_path).is_dir():
                print("Common Metadata Across All Files:")
                print("-" * 40)
            pprint(common_metadata)
            print("-" * 40)
        else:
            print("No Common Metadata Across All Files")
            print("-" * 40)

        # Print unique parameters for each file
        for i, d in enumerate(metadata):
            unique_metadata = {key: value for key, value in d.items() if
                               key not in common_metadata and key not in ignore_keys}
            if unique_metadata:
                print(f"Metadata for File {i + 1} ({os.path.relpath(d.get('SourceFile', 'Unknown'), start=folder_path)}):")
                print("-" * 40)
                pprint(unique_metadata)
                print("-" * 40)


setup(
    group="visualization",
    name="inspect-metadata-exiftool",
    version="0.1.0",
    title="Image metadata inspection with exiftool",
    description="This solution prints what is provided by exiftool either for single files or recursively for whole folders. Information is condensed if all files have the same metadata for certain tags.",
    solution_creators=["Deborah Schmidt"],
    cite=[{
        "text": "ExifTool by Phil Harvey",
        "url": "https://exiftool.org/"
    }],
    tags=["visualization", "metadata"],
    license="MIT",
    covers=[{
        "description": "The text output of the solution depicting the metadata information of TIFF files provided by exiftool.",
        "source": "cover.jpg"
    }],
    album_api_version="0.5.5",
    args=[{
        "name": "input",
        "type": "file",
        "description": "The image or folder to analyze metadata from.",
        "required": True
    }, {
        "name": "file_pattern",
        "type": "string",
        "description": "Regex for matching specific files, by default matches all files. Use for example \"*.tif\" for all TIFFs in the folder.",
        "default": "*.*"
    }, {
        "name": "ignore_keys",
        "type": "string",
        "description": "Comma separated list of metadata keys which should not be printed.",
        "default": "File:Directory,SourceFile"
    }],
    run=run,
    dependencies={'environment_file': """channels:
  - conda-forge
  - defaults
dependencies:
  - python=3.9
  - exiftool=12.42
  - pip
  - pip:
    - pyexiftool==0.5.5
"""}
)
