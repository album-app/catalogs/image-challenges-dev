from album.runner.api import setup


def install():
    import subprocess
    import shutil
    from album.runner.api import get_app_path, get_package_path

    get_app_path().mkdir(exist_ok=True, parents=True)

    # copy source files into solution app folder
    shutil.copy(get_package_path().joinpath('build.gradle'), get_app_path())
    shutil.copy(get_package_path().joinpath('gradlew'), get_app_path())
    shutil.copy(get_package_path().joinpath('gradlew.bat'), get_app_path())
    shutil.copytree(get_package_path().joinpath('src'), get_app_path().joinpath('src'))
    shutil.copytree(get_package_path().joinpath('gradle'), get_app_path().joinpath('gradle'))

    subprocess.run([(get_gradle_executable()), 'build', '-Dorg.gradle.internal.http.socketTimeout=300000'],
                   cwd=get_app_path(), check=True)


def get_gradle_executable():
    from sys import platform
    from album.runner.api import get_app_path
    if platform == "win32":
        return str(get_app_path().joinpath('gradlew.bat').absolute())
    return str(get_app_path().joinpath('gradlew').absolute())


def run():
    import subprocess
    from album.runner.api import get_args, get_app_path
    from pathlib import Path
    args = get_args()
    input = args.input
    animation_file = args.animation_file
    output = args.output
    if input:
        input = Path(input).absolute()
    if output:
        output = Path(output).absolute()
    if animation_file:
        animation_file = Path(animation_file).absolute()
    command = [get_gradle_executable(), 'run', '-q', '--args="%s,%s,%s,%s,%s"' % (
        input, animation_file, args.width, args.height, output)]
    subprocess.run(command, cwd=get_app_path())


setup(
    group="visualization",
    name="animate-with-3dscript",
    version="0.1.0",
    solution_creators=["Deborah Schmidt"],
    title="Animate 3D volume rendering with 3Dscript",
    description="This solution launches 3Dscript in Fiji with the provided 3D image stack.",
    tags=["3dscript", "fiji", "animation", "visualization"],
    cite=[{
        "text": "Schmid, B.; Tripal, P. & Fraaß, T. et al. (2019), \"3Dscript: animating 3D/4D microscopy data using a natural-language-based syntax\", Nature methods 16(4): 278-280, PMID 30886414. ",
        "doi": "10.1038/s41592-019-0359-1",
        "url": "https://bene51.github.io/3Dscript/"
    }],
    album_api_version="0.5.5",
    args=[{
            "name": "input",
            "type": "file",
            "required": True,
            "description": "The image to animate (3D)."
        }, {
            "name": "animation_file",
            "type": "file",
            "required": False,
            "description": "The animation to display or render."
        }, {
            "name": "width",
            "type": "integer",
            "default": 800,
            "description": "The width of the rendering."
        }, {
            "name": "height",
            "type": "integer",
            "default": 600,
            "description": "The height of the rendering."
        }, {
            "name": "output",
            "type": "file",
            "required": False,
            "description": "Where the animation should be stored to - if this parameter is provided, the interactive mode will not be opened, the rendering will be generated headlessly."
        }],
    covers=[{
        "description": "The interface of 3Dscript and the 3D rendering view of a generative dataset.",
        "source": "cover.jpg"
    }],
    install=install,
    run=run,
    dependencies={'parent': {'resolve_solution': 'visualization:launch-fiji:0.1.0'}}
)

