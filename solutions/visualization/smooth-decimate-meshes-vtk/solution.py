from album.runner.api import setup

# Please import additional modules at the beginning of your method declarations.
# More information: https://docs.album.solutions/en/latest/solution-development/


def run():
    import vtk
    import glob
    import os
    import re
    from pathlib import Path

    from album.runner.api import get_args

    args = get_args()
    smoothing_iterations = args.smoothing_iterations
    smoothing_feature_angle = args.smoothing_feature_angle
    smoothing_pass_band = args.smoothing_pass_band
    decimate_percentage = 1 - args.decimate_ratio

    def process_mesh(mesh, out_path):
        reader = vtk.vtkSTLReader()
        reader.SetFileName(mesh)
        reader.Update()
        next_input = reader.GetOutput()

        if smoothing_iterations > 0:
            smoother = vtk.vtkWindowedSincPolyDataFilter()
            smoother.SetInputData(next_input)
            smoother.SetNumberOfIterations(smoothing_iterations)
            smoother.BoundarySmoothingOn()
            smoother.FeatureEdgeSmoothingOn()
            smoother.SetFeatureAngle(smoothing_feature_angle)
            smoother.SetPassBand(smoothing_pass_band)
            smoother.NonManifoldSmoothingOn()
            smoother.NormalizeCoordinatesOn()
            smoother.Update()
            next_input = smoother.GetOutput()

        if decimate_percentage > 0:
            decimate = vtk.vtkDecimatePro()
            decimate.SetInputData(next_input)
            decimate.SetTargetReduction(decimate_percentage)
            decimate.PreserveTopologyOn()
            decimate.Update()

            next_input = decimate.GetOutput()
            # decimated = vtk.vtkPolyData()
            # decimated.ShallowCopy(decimate.GetOutput())
            # print('Before decimation:')
            # print(f'There are {decimated.GetNumberOfPoints()} points.')
            # print(f'There are {decimated.GetNumberOfPolys()} polygons.')
            # print(
            #     f'Reduction: {(shape.GetNumberOfPolys() - decimated.GetNumberOfPolys()) / shape.GetNumberOfPolys()}')
        # # Compute Normals
        # normals_generator = vtk.vtkPolyDataNormals()
        # normals_generator.SetInputData(next_input)
        # normals_generator.ComputePointNormalsOn()  # Compute Point Normals
        # normals_generator.ComputeCellNormalsOn()  # If needed, Compute Cell Normals too
        # normals_generator.Update()
        #
        # # Replace the next_input with the output from normals_generator
        # next_input = normals_generator.GetOutput()
        # Create PolyData writer
        writer = vtk.vtkSTLWriter()
        writer.SetFileName(out_path)
        writer.SetInputData(next_input)
        writer.SetFileTypeToBinary()
        writer.Write()
        writer.Update()
        return out_path

    meshes_folder = args.input
    output_folder = args.output
    Path(output_folder).mkdir(parents=True, exist_ok=True)
    colormap = args.colormap
    exclude_zero = args.exclude_zero
    show_plot = args.show_meshes
    include = args.include
    exclude = args.exclude
    if include:
        include = include.split(",")

    if exclude:
        exclude = exclude.split(",")

    all_meshes = []
    pattern = re.compile(r'([^/]+?)(?:_(\d+))?\..*$')
    for i, vtk_file in enumerate(glob.glob(f"{meshes_folder}/*.stl")):
        if include:
            if not any(_include in str(vtk_file) for _include in include):
                continue
        if exclude:
            if any(_exclude in str(vtk_file) for _exclude in exclude):
                continue
        match = pattern.search(vtk_file)
        region_id = None
        if match:
            base_name, region_id = match.groups()
        if region_id and int(region_id) == 0 and exclude_zero:
            continue
        output_mesh = process_mesh(vtk_file, os.path.join(str(output_folder), os.path.basename(vtk_file)))
        if show_plot:
            all_meshes.append(output_mesh)

    if show_plot:
        renderer = add_meshes_as_actors(all_meshes, colormap)
        renderWindow = vtk.vtkRenderWindow()

        renderWindow.AddRenderer(renderer)
        render_window_interactor = vtk.vtkRenderWindowInteractor()
        render_window_interactor.SetRenderWindow(renderWindow)

        render_window_interactor.Start()


def add_meshes_as_actors(meshes_files, colormap):
    import vtk
    import matplotlib.pyplot as plt
    import matplotlib.colors as mcolors  # For custom colormap
    # Choose colormap
    cmap = plt.get_cmap(colormap)
    # Create a lookup table for coloring
    # lut = vtk.vtkLookupTable()
    # lut.SetNumberOfTableValues(256)
    # lut.Build()
    # Initialize the colormap and normalizer
    renderer = vtk.vtkRenderer()

    norm = mcolors.Normalize(vmin=0, vmax=len(meshes_files))

    for i, vtk_file in enumerate(meshes_files):
        # Load the VTK file
        reader = vtk.vtkSTLReader()
        reader.SetFileName(vtk_file)
        reader.Update()
        # Create mapper and actor
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputConnection(reader.GetOutputPort())

        actor = vtk.vtkActor()
        actor.SetMapper(mapper)

        color_rgba = cmap(norm(int(i)))
        actor.GetProperty().SetColor(color_rgba[:3])

        renderer.AddActor(actor)
    return renderer

setup(
    group="visualization",
    name="smooth-decimate-meshes-vtk",
    version="0.1.0",
    title="Smooth and decimate meshes in VTK",
    description="The provided meshes can be filtered, then smoothed and reduced with adjustable parameters, based the VTK, and optionally displayed.",
    solution_creators=["Deborah Schmidt"],
    tags=[],
    cite=[{
        "text": "Schroeder, Will; Martin, Ken; Lorensen, Bill (2006), The Visualization Toolkit (4th ed.), Kitware, ISBN 978-1-930934-19-1 ",
        "url": "https://vtk.org"
    }],
    license="MIT",
    album_api_version="0.5.5",
    covers=[{
        "description": "Three outputs of the solution generating meshes with different parameter values for smoothing and reduction.",
        "source": "cover.jpg"
    }],
    args=[{
            "name": "input",
            "type": "directory",
            "required": True,
            "description": "Path to the meshes to process."
        }, {
            "name": "output",
            "type": "directory",
            "required": True,
            "description": "Path where the smoothed and reduced meshes should be stored to. The meshes will keep their original file names."
        }, {
            "name": "include",
            "type": "string",
            "description": "List of names of elements which should be loaded, comma separated",
            "required": False
        }, {
            "name": "exclude",
            "type": "string",
            "description": "List of names of elements which should not be loaded, comma separated",
            "required": False
        }, {
            "name": "smoothing_iterations",
            "type": "integer",
            "default": 2,
            "description": "Smoothing iterations. Set to 0 to skip smoothing."
        }, {
            "name": "smoothing_pass_band",
            "type": "float",
            "default": 0.1,
            "description": "Smoothing pass band."
        }, {
            "name": "smoothing_feature_angle",
            "type": "float",
            "default": 120.,
            "description": "Smoothing feature angle."
        }, {
            "name": "decimate_ratio",
            "type": "float",
            "default": 0.5,
            "description": "Sets the ratio of to which percentage to reduce the vertices to (0-1, 1 means do not decimate)."
        }, {
            "name": "show_meshes",
            "type": "boolean",
            "default": False,
            "description": "Whether to display the generated mesh plots or not."
        }, {
            "name": "colormap",
            "type": "string",
            "default": "viridis",
            "description": "Name of the matplotlib colormap to use for coloring meshes."
        }, {
            "name": "exclude_zero",
            "type": "boolean",
            "default": True,
            "description": "Exclude 0 as a label when processing the meshes. This means all mesh files where the file ends with \"_0.stl\" will be ignored."
        }
    ],
    run=run,
    dependencies={'parent': {'resolve_solution': 'visualization:visualize-meshes-vtk:0.1.0-SNAPSHOT'}}
)