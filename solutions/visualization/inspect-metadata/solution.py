from album.runner.api import setup

# Please import additional modules at the beginning of your method declarations.
# More information: https://docs.album.solutions/en/latest/solution-development/

def run():
    import exiftool
    import glob
    from pprint import pprint
    from collections import defaultdict
    from album.runner.api import get_args

    # Replace this with the path to your folder
    folder_path = get_args().input
    file_pattern = get_args().file_pattern
    ignore_keys = get_args().ignore_keys

    # This will recursively find all .tif files within the specified folder and its subfolders
    files = glob.glob(f"{folder_path}/**/{file_pattern}", recursive=True)

    with exiftool.ExifToolHelper() as et:
        metadata = et.get_metadata(files)

        # Identify common parameters with the same values across all files
        common_metadata = defaultdict(list)

        for d in metadata:
            for key, value in d.items():
                if key not in ignore_keys:
                    common_metadata[key].append(value)

        common_metadata = {key: values[0] for key, values in common_metadata.items() if all(x == values[0] for x in values)}

        # Print common parameters
        print("Common Metadata Across All Files:")
        print("-" * 40)
        pprint(common_metadata)
        print("-" * 40)

        # Print unique parameters for each file
        for i, d in enumerate(metadata):
            # Remove common and ignored metadata from each file's metadata
            unique_metadata = {key: value for key, value in d.items() if key not in common_metadata and key not in ignore_keys}

            if unique_metadata:  # Only print if there are unique metadata
                print(f"Unique Metadata for File {i+1} ({d.get('SourceFile', 'Unknown')}):")
                print("-" * 40)
                pprint(unique_metadata)
                print("-" * 40)


setup(
    group="visualization",
    name="inspect-metadata",
    version="0.1.0-SNAPSHOT",
    title="Image metadata observation",
    description="A solution for metadata extraction of single images or image folder with exiftool.",
    solution_creators=["Deborah Schmidt"],
    cite=[],
    tags=["visualization", "metadata"],
    license="MIT",
    covers=[],
    album_api_version="0.6.0",
    args=[{
        "name": "input",
        "type": "string",
        "description": "The image or folder to analyze metadata from.",
        "required": True
    }, {
        "name": "file_pattern",
        "type": "string",
        "description": "Regex for matching specific files, by default matches all files. Use for example \"*.tif\" for all TIFFs in the folder.",
        "default": "*.*"
    }, {
        "name": "ignore_keys",
        "type": "string",
        "description": "Comma separated list of metadata keys which should not be printed.",
        "default": "File:FileAccessDate,File:FileInodeChangeDate,File:FileModifyDate,File:FileName,File:FileSize,SourceFile,EXIF:StripByteCounts,EXIF:StripOffsets"
    }],
    run=run,
    dependencies={'environment_file': """channels:
  - conda-forge
  - defaults
dependencies:
  - python=3.9
  - exiftool=12.42
  - pip
  - pip:
    - pyexiftool==0.5.5
"""}
)
