from album.runner.api import setup

# Define the conda environment as a YAML string
env_file = """
channels:
  - conda-forge
dependencies:
  - python=3.9
  - pip
  - git
  - pip:
    - git+https://github.com/OsaAjani/moviepy.git
"""

def run():
    from album.runner.api import get_args
    import os
    from moviepy.video.io.ImageSequenceClip import ImageSequenceClip

    args = get_args()

    input_dir = args.input_dir
    output_file = args.output_file
    frame_rate = args.frame_rate
    resolution = args.resolution
    image_format = args.image_format

    # Validate input directory
    if not os.path.isdir(input_dir):
        raise FileNotFoundError(f"Input directory '{input_dir}' does not exist.")

    # Get list of image files
    images = sorted([
        os.path.join(input_dir, img)
        for img in os.listdir(input_dir)
        if img.lower().endswith(f".{image_format.lower()}")
    ])

    if not images:
        raise ValueError(f"No images with format '.{image_format}' found in '{input_dir}'.")

    print(f"Found {len(images)} images. Creating video...")

    # Create video clip
    clip = ImageSequenceClip(images, fps=frame_rate)

    # # Resize if resolution is specified
    # if resolution:
    #     width, height = map(int, resolution.split('x'))
    #     clip = clip.resize(newsize=(width, height))

    # Write the video file
    clip.write_videofile(output_file, codec='libx264', audio=False)

    print(f"Video successfully created at '{output_file}'")

def test():
    # Implement test logic if needed
    pass

# Album setup
setup(
    group="visualization",
    name="generate-video-from-images",
    version="0.1.0-SNAPSHOT",
    album_api_version="0.5.5",  # Replace with your Album API version
    title="Generate Video from Images",
    description="Creates a video from a folder of images.",
    solution_creators=["Deborah Schmidt"],
    tags=["video", "images", "moviepy"],
    license="MIT",
    args=[
        {
            "name": "input_dir",
            "type": "string",
            "description": "Directory containing input images.",
            "required": True,
        },
        {
            "name": "output_file",
            "type": "string",
            "description": "Output video file path (e.g., output.mp4).",
            "required": True,
        },
        {
            "name": "frame_rate",
            "type": "float",
            "description": "Frame rate of the output video.",
            "default": 30.0,
        },
        {
            "name": "resolution",
            "type": "string",
            "description": "Resolution of the output video (e.g., 1920x1080).",
            "default": "1920x1080",
        },
        {
            "name": "image_format",
            "type": "string",
            "description": "Image file format without dot (e.g., jpg, png).",
            "default": "jpg",
        },
    ],
    dependencies={"environment_file": env_file},
    run=run,
    test=test,
)
