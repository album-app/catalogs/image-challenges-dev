from album.runner.api import setup

# Please import additional modules at the beginning of your method declarations.
# More information: https://docs.album.solutions/en/latest/solution-development/


def run():
    import napari
    from skimage.io import imread
    from album.runner.api import get_args

    input = get_args().input
    if input:
        img = imread(input)
        napari.view_image(img, rgb=get_args().rgb, name="Input image")
    else:
        napari.Viewer()
    napari.run()


setup(
    group="visualization",
    name="launch-napari",
    version="0.1.0",
    title="Launch napari v0.4.18",
    description="This solution launches napari with the option to directly open a dataset.",
    solution_creators=["Deborah Schmidt"],
    cite=[{
        "text": "napari contributors (2019). napari: a multi-dimensional image viewer for python.",
        "doi": "10.5281/zenodo.3555620",
        "url": "https://github.com/napari/napari"
    }],
    tags=["template", "napari"],
    license="MIT",
    covers=[{
        "description": "Napari displaying two generative 3D datasets, one as a label image, the other one as a via attenuated MIP.",
        "source": "cover.jpg"
    }],
    album_api_version="0.5.1",
    args=[{
        "name": "input",
        "type": "file",
        "description": "The image about to be displayed in napari.",
        "required": False
    }, {
        "name": "rgb",
        "type": "boolean",
        "description": "Display the image as an RGB image",
        "default": False
    }],
    run=run,
    dependencies={'environment_file': """channels:
  - conda-forge
  - defaults
dependencies:
  - python=3.9
  - scikit-image=0.21.0
  - pyqt=5.15.7
  - pip
  - pip:
    - napari==0.4.18
"""}
)
