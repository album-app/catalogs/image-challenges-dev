# StarDist Model Training Solution for Album

## Introduction
MicroSam is a tool for segmentation and tracking in microscopy and build on top of SegmentAnything.
Please have a look at the official [SegmentAnything for microscopy documentation](https://github.com/computational-cell-analytics/micro-sam) for further information.

## Installation
Make sure album is already installed. If not, download and install it as described [here](https://album.solutions/).
Also, don't forget to add the catalog to your album installation, so you can install the solutions from the catalog.

Install the `stardist_train` and `stardist_predict` solution by using the graphical user interface (GUI) of album or by running the following command in the terminal:
```bash
album install segmentation:microSAM_predict:0.1.0
```

## How to use
# todo: add more details


### Further documentation: 
For further options, parameters and default values, please refer to the info page of the solution:
```bash
album info microSAM_predict
```

## Hardware Requirements
Usually a mimimum of 8GB GPU RAM is required to run the solution. 


## Citation & License
Stardist is licensed under the [MIT License](https://opensource.org/license/mit/).

If you use this solution, please cite the following paper:

```
title: Segment Anything for Microscopy,
doi: 10.1101/2023.08.21.554208
```
and
```
title: Segment Anything,
doi: 10.48550/arXiv.2304.02643
```
