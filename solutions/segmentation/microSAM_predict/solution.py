"""Module holding the SAM segmentation solution."""
from album.runner.api import get_args, get_cache_path, setup

# DO NOT IMPORT ANYTHING OTHER THAN FROM RUNNER API HERE

env_file = """name:  microSAM
channels:
  - pytorch
  - nvidia
  - conda-forge
dependencies:
  - python=3.10
  - tifffile>=2023.7.18
  - pytorch>=2.0.0
  - torchvision>=0.15.0
  - pytorch-cuda=11.7
  - micro_sam=0.3.0pos2
  - pip
"""


def setup_album():
    """Initialize the album api."""
    from album.api import Album

    album_api = Album.Builder().build()
    album_api.load_or_create_collection()

    return album_api


def run():
    """Python code that is executed in the solution environment."""
    # parse arguments
    args = get_args()

    # cache path
    cache_path = get_cache_path()

    import os

    import numpy as np
    import tifffile
    from micro_sam import instance_segmentation, util
    from micro_sam.util import get_sam_model

    # open tiff
    img_channel = tifffile.TiffFile(args.input_path).asarray()

    embedding_path = args.embedding_path
    if (
        args.embedding_path is None
        or args.embedding_path == "None"
        or args.embedding_path == ""
    ):
        embedding_path = str(cache_path.joinpath("embeddings.zarr"))

    checkpoint_path = args.checkpoint_path
    if (
        args.checkpoint_path is None
        or args.checkpoint_path == "None"
        or args.checkpoint_path == ""
    ):
        checkpoint_path = None

    sam_pred = get_sam_model(
        model_type=args.model_name, checkpoint_path=checkpoint_path
    )
    amg = instance_segmentation.AutomaticMaskGenerator(sam_pred)
    embeddings = util.precompute_image_embeddings(
        sam_pred, img_channel, save_path=embedding_path
    )
    amg.initialize(img_channel, embeddings, verbose=True)
    instances_amg = amg.generate(pred_iou_thresh=args.pred_iou_thresh)
    instances_amg = instance_segmentation.mask_data_to_segmentation(
        instances_amg, shape=img_channel.shape, with_background=True
    )

    # save masks
    out_file = os.path.join(args.output_path, "mask.npy")
    np.save(out_file, instances_amg, allow_pickle=True)

    # save as image
    out_file = os.path.join(args.output_path, "mask.tif")
    tifffile.imwrite(out_file, instances_amg.astype(np.uint8))


setup(
    group="segmentation",
    name="microSAM_predict",
    version="0.1.0",
    title="Segment Anything for Microscopy",
    description="A solution to create segmentations with the previously trained "
    "microSAM model fintetuned from the Segment Anything model from facebookresearch.",
    solution_creators=["Jan Philipp Albrecht"],
    cite=[{
        "text": "Archit, Anwai and Nair, Sushmita and Khalid, Nabeel and Hilt, Paul and Rajashekar, Vikas and Freitag, Marei and Gupta, Sagnik and Dengel, Andreas and Ahmed, Sheraz and Pape, Constantin (2023). Segment Anything for Microscopy. bioRxiv.",
        "doi": "10.1101/2023.08.21.554208"
    }],
    tags=[
        "segmentation",
        "machine_learning",
        "images",
        "SAM",
        "facebook",
        "anything",
        "microSAM",
    ],
    license="MIT",
    documentation=["https://github.com/computational-cell-analytics/micro-sam.git"],
    covers=[],
    album_api_version="0.5.5",
    args=[
        {
            "name": "input_path",
            "type": "string",
            "required": True,
            "description": "Path (file) to the 1 channel (greyscale) image to be segmented.",
        },
        {
            "name": "output_path",
            "type": "string",
            "required": True,
            "description": "Path (directory) where the result mask will be stored.",
        },
        {
            "name": "model_name",
            "type": "string",
            "required": False,
            "description": "The name to the model to use.",
            "default": "vit_h",
        },
        {
            "name": "embedding_path",
            "type": "string",
            "required": False,
            "description": "Path (directory) where the image embedding model is stored.",
        },
        {
            "name": "checkpoint_path",
            "type": "string",
            "required": False,
            "description": "Path (file) to the checkpoint of the model to use. If none is given default url is used based on your model name.",  # noqa: E501
            "default": None,
        },
        {
            "name": "pred_iou_thresh",
            "type": "float",
            "required": False,
            "description": "The models own prediction of the masks quality. Quality is filtered by this parameter.",
            "default": 0.88,
        },
    ],
    run=run,
    dependencies={"environment_file": env_file},
)