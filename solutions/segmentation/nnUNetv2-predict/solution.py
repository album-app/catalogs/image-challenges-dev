from album.runner.api import get_args, setup

def run():
    import os
    import sys
    import subprocess
    from pathlib import Path
    args = get_args()
    # set environment variables before import
    os.environ['nnUNet_raw'] = args.data
    os.environ['nnUNet_preprocessed'] = args.preprocessed
    os.environ['nnUNet_results'] = args.results_folder
    os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'
    os.environ['nnUNet_def_n_proc'] = '1' 

    from nnunetv2.inference.predict_from_raw_data import predict_entry_point

    # convert args to sys.argv
    sys.argv = [sys.argv[0]] + [
        "-i", args.input_path,
        "-o", args.output_folder,
    ]

    if args.dataset_name_or_id != "":
        sys.argv += ["-d", args.dataset_name_or_id]

    if args.fold != "all":
        sys.argv += ["-f", args.fold]

    if args.step_size != 0.5:
        sys.argv += ["-step_size", str(args.step_size)]

    if args.tr != "nnUNetTrainer":
        sys.argv += ["-tr", args.tr]

    if args.configuration != "":
        sys.argv += ["-c", args.configuration]

    if args.p != "nnUNetPlans":
        sys.argv += ["-p", args.p]

    if args.disable_tta in [True, "True", "true", "1"]:
        sys.argv += ["--disable_tta"]

    if args.verbose in [True, "True", "true", "1"]:
        sys.argv += ["--verbose"]

    if args.save_probabilities in [True, "True", "true", "1"]:
        sys.argv += ["--save_probabilities"]

    if args.continue_prediction in [True, "True", "true", "1"]:
        sys.argv += ["--continue_prediction"]

    if args.checkpoint_name != "model_final_checkpoint.pth":
        sys.argv += ["-chk", args.checkpoint_name]

    if args.npp != "":
        sys.argv += ["-npp", str(args.npp)]

    if args.nps != "":
        sys.argv += ["-nps", str(args.nps)]

    if args.device != "cuda":
        sys.argv += ["-device", args.device]

    subprocess.run([sys.executable, Path(__file__).parent / "nnUnetv2_predict.py"] + sys.argv[1:])

    return


setup(
    group="segmentation",
    name="nnUNetv2-predict",
    version="0.1.0",
    title="nnUNetv2 UNet predict",
    description="A solution to predict with the previously trained nnUNetv2 UNet model",
    solution_creators=["Jan Philipp Albrecht", "Maximilian Otto"],
    cite=[{
        "text": "Isensee, F., Jaeger, P. F., Kohl, S. A., Petersen, J., & Maier-Hein, K. H. (2021). nnU-Net: a self-configuring method for deep learning-based biomedical image segmentation. Nature methods, 18(2), 203-211.",
        "doi": "10.1038/s41592-020-01008-z"
    }],
    tags=["unet", "machine_learning", "images"],
    license="Apache v2",
    documentation=["https://github.com/MIC-DKFZ/nnUNet"],
    covers=[],
    album_api_version="0.5.5",
    args=[
        {
            "name": "data",
            "type": "string",
            "required": True,
            "description": "[REQUIRED] Path to the folder containing the raw data. This folder must contain a subfolder for each dataset. Each dataset folder must contain a folder \"imagesTr\" with the training images and a folder \"labelsTr\" with the corresponding training labels. The test images must be in a folder \"imagesTs\"."
        },
        {
            "name": "preprocessed",
            "type": "string",
            "required": True,
            "description": "[REQUIRED] Path to the folder that got created when planning the experiment."
        },
        {
            "name": "results_folder",
            "type": "string",
            "required": True,
            "description": "[REQUIRED] Path to the folder containing the trained networks. If it does not exists, run nnUNetv2_train first."
        },
        {
            "name": "input_path",
            "type": "string",
            "required": True,
            "description": "[REQUIRED] Path to the specific input folder path you want to predict. TIF format with the same file naming convention as during training is expected."
        },
        {
            "name": "output_folder",
            "type": "string",
            "required": True,
            "description": "[REQUIRED] Desired output path for the predicted segmentations."
        },
        {
            "name": "dataset_name_or_id",
            "type": "string",
            "required": True,
            "description": "[REQUIRED] Dataset name or ID to predict."
        },
        {
            "name": "configuration",
            "type": "string",
            "required": True,
            "description": "[REQUIRED] nnU-Net configuration that should be used for prediction (e.g. `2d`, `3d_fullres`, etc.)."
        },
        {
            "name": "fold",
            "type": "string",
            "required": False,
            "description": "[OPTIONAL] Folds of the trained model used for prediction. Should be given as integers between 0 and 4. Default: all"
        },
        {
            "name": "step_size",
            "type": "float",
            "required": False,
            "default": 0.5,
            "description": "[OPTIONAL] Step size of the sliding window used for prediction. Increase to makeit faster but less accurate. Expects a floating point number between 0 and 1. The default of 0.5 is recommended."
        },
        {
            "name": "tr",
            "type": "string",
            "required": False,
            "default": "nnUNetTrainer",
            "description": "[OPTIONAL] Use this flag to specify a custom trainer. Default: nnUNetTrainer"
        },
        {
            "name": "p",
            "type": "string",
            "required": False,
            "default": "nnUNetPlans",
            "description": "[OPTIONAL] Use this flag to specify a custom plans identifier. Default: nnUNetPlans"
        },
        {
            "name": "disable_tta",
            "type": "boolean",
            "required": False,
            "default": False,
            "description": "[OPTIONAL] Set this flag to disable test time data augmentation (mirroring). Faster, but less accurate. Not recommended. Default: False"
        },
        {
            "name": "save_probabilities",
            "type": "boolean",
            "required": False,
            "default": False,
            "description": "[OPTIONAL] If True, the probabilities will be saved as well. If you plan to use ensembling, it is necessary. Default: False"
        },
        {
            "name": "verbose",
            "type": "boolean",
            "required": False,
            "default": False,
            "description": "[OPTIONAL] This flag prints more information during prediction. Default: False"
        },
        {
            "name": "continue_prediction",
            "type": "boolean",
            "required": False,
            "default": False,
            "description": "[OPTIONAL] Continue an aborted previous prediction (will not overwrite existing files). Default: False"
        },	
        {
            "name": "checkpoint_name",
            "type": "string",
            "required": False,
            "default": "model_final_checkpoint.pth",
            "description": "[OPTIONAL] Name of the checkpoint you want to use. Default: model_final_checkpoint.pth"
        },
        {
            "name": "device",
            "type": "string",
            "default": "cuda",
            "required": False,
            "description": "[OPTIONAL] Use this to set the device the inference should run on. Available options are \"cuda\" (GPU), \"cpu\" (CPU) and \"mps\" (Apple M1/M2). Do NOT use this to set which GPU ID! Use CUDA_VISIBLE_DEVICES=X nnUNetv2_train [...] instead! Default: cuda"
        },
        {
            "name": "nps",
            "type": "integer",
            "default": 3,
            "required": False,
            "description": "[OPTIONAL] Number of processes used for segmentation export. More is not always better. Beware of out-of-RAM issues. Default: 3"
        },
        {
            "name": "npp",
            "type": "integer",
            "default": 3,
            "required": False,
            "description": "[OPTIONAL] Number of processes used for preprocessing. More is not always better. Beware of out-of-RAM issues. Default: 3"        
        }
    ],
    run=run,
    dependencies={
        "parent": {
            "group": "segmentation",
            "name": "nnUNetv2-parent",
            "version": "0.1.0"
        },
    }
)
