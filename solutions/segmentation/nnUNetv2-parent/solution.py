import sys 
from album.runner.api import setup 

if (sys.platform != "win32"):
  env_file = """name: nnunetv2
channels: 
  - conda-forge
  - defaults
dependencies:
  - python>=3.9
  - cudatoolkit=11.7
  - matplotlib=3.5
  - pip
  - pip:
    - torch>=2.0.0
    - torchvision
    - torchaudio
    - nnunetv2
    - hiddenlayer
    - IPython
  """
else:
  env_file = """name:  nnunetv2
channels: 
  - pytorch
  - nvidia
  - conda-forge
dependencies: 
  - python=3.9
  - matplotlib=3.5
  - pytorch>=2.0.0
  - torchvision>=0.15.0
  - torchaudio>=2.0.0
  - pytorch-cuda=11.7
  - pip
  - pip:
    - nnunetv2==2.1
    - hiddenlayer
    - IPython
  """


setup(
    group="segmentation",
    name="nnUNetv2-parent",
    version="0.1.0",
    title="nnunet base solution",
    description="A solution to provide the nnunet environment",
    solution_creators=["Jan Philipp Albrecht", "Lucas Rieckert", "Maximilian Otto"],
    cite=[{
        "text": "Isensee, F., Jaeger, P. F., Kohl, S. A., Petersen, J., & Maier-Hein, K. H. (2021). nnU-Net: a self-configuring method for deep learning-based biomedical image segmentation. Nature methods, 18(2), 203-211.",
        "doi": "10.1038/s41592-020-01008-z"
    }],
    tags=["unet", "machine_learning", "images"],
    license="Apache v2",
    documentation=["https://github.com/MIC-DKFZ/nnUNet"],
    covers=[],
    album_api_version="0.5.5",
    args=[],
    dependencies={"environment_file": env_file}
)
