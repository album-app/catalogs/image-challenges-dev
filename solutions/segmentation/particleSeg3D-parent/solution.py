from album.runner.api import setup 

env_file = """name:  particleSeg3D
channels:
  - pytorch
  - nvidia
  - conda-forge
  - defaults
dependencies:
  - python=3.9
  - pytorch>=2.0.0
  - torchvision>=0.15.0
  - torchaudio>=2.0.0
  - pytorch-cuda=11.7
  - pip=23.2.1
  - numpy=1.25.2
  - pip:
    - ParticleSeg3D==0.2.12
"""

setup(
    group="segmentation",
    name="particleSeg3D-parent",
    version="0.1.0",
    title="particleSeg3D base solution",
    description="A solution to provide the particleSeg3D environment",
    solution_creators=["Jan Philipp Albrecht", "Maximilian Otto"],
    cite=[{
        "text": "[Work in progress] Scalable, out-of-the box segmentation of individual particles from mineral samples acquired with micro CT, Karol Gotkowski and Shuvam Gupta and Jose R. A. Godinho and Camila G. S. Tochtrop and Klaus H. Maier-Hein and Fabian Isensee, 2023",
        "doi": "tbd"
    }],
    tags=["unet", "machine_learning", "images", "segmentation", "particleSeg3D", "3D"],
    license="Apache v2",
    documentation=["https://github.com/MIC-DKFZ/ParticleSeg3D/tree/main"],
    covers=[],
    album_api_version="0.5.5",
    args=[],
    dependencies={"environment_file": env_file}
)
