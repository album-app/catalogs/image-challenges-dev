from album.runner.api import setup 

env_file = """name:  DeepCell
channels: 
  - pytorch
  - nvidia
  - conda-forge
dependencies: 
  - python=3.9
  - tifffile>=2023.7.18
  - cudatoolkit=11.2  
  - cudnn=8.1  
  - pip
  - pip:
    - deepcell==0.12.7
    """


setup(
    group="segmentation",
    name="DeepCell-tf-parent",
    version="0.1.0",
    title="DeepCell base solution",
    description="A solution to provide the DeepCell-tf environment",
    solution_creators=["Maximilian Otto", "Jan Philipp Albrecht"],
    cite=[{
        "text": "Noah F. Greenwald and Geneva Miller and Erick Moen and Alex Kong and Adam Kagel and Thomas Dougherty and Christine Camacho Fullaway and Brianna J. McIntosh and Ke Xuan Leow and Morgan Sarah Schwartz and Cole Pavelchek and Sunny Cui and Isabella Camplisson and Omer Bar-Tal and Jaiveer Singh and Mara Fong and Gautam Chaudhry and Zion Abraham and Jackson Moseley and Shiri Warshawsky and Erin Soon and Shirley Greenbaum and Tyler Risom and Travis Hollmann and Sean C. Bendall and Leeat Keren and William Graf and Michael Angelo and David Van Valen; Whole-cell segmentation of tissue images with human-level performance using large-scale data annotation and deep learning",
        "doi": "https://doi.org/10.1038/s41587-021-01094-0"
    }],
    tags=["DeepCell", "machine_learning", "images", "segmentation", "single_cell"],
    license="Modified Apache v2",
    documentation=["https://deepcell.readthedocs.io/en/master/"],
    covers=[{
        "description": "DeepCell Core Library Logo",
        "source": "cover.png"
    }],
    album_api_version="0.5.5",
    args=[],
    dependencies={"environment_file": env_file}
)
