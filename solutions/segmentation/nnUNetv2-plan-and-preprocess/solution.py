from album.runner.api import get_args, setup


def run():
    import os
    import sys

    # get solution arguments
    args = get_args()

    # set environment variables for nnUNet
    os.environ['nnUNet_raw'] = args.data
    os.environ['nnUNet_preprocessed'] = args.preprocessed

    # import after setting environment variables
    from nnunetv2.experiment_planning.plan_and_preprocess_entrypoints import plan_and_preprocess_entry

    # convert args to sys.argv
    sys.argv = [sys.argv[0]] + \
               ["-d"] + str(args.id).split(",") + \
               [
                   "-fpe", args.fpe,
                   "-npfp", str(args.npfp),
                   "-pl", args.pl,
                   "-gpu_memory_target", str(args.gpu_memory_target),
                   "-preprocessor_name", args.preprocessor_name,
               ]
    if args.overwrite_plans_name != "None":
        sys.argv += ["-overwrite_plans_name", args.overwrite_plans_name]

    # nargs+ optional arguments
    if args.c != "None":
        sys.argv += ["-c"] + str(args.c).split(",")

    if args.np != "None":
        sys.argv += ["-np"] + str(args.np).split(",")

    if args.overwrite_target_spacing != "None":
        sys.argv += ["-overwrite_target_spacing"] + str(args.overwrite_target_spacing).split(",")

    # flag arguments
    if args.verify_dataset_integrity in ["True", "true", "1"]:
        sys.argv += ["--verify_dataset_integrity"]

    if args.no_pp in ["True", "true", "1"]:
        sys.argv += ["--no_pp"]

    if args.clean in ["True", "true", "1"]:
        sys.argv += ["-clean"]

    if args.verbose in ["True", "true", "1"]:
        sys.argv += ["--verbose"]

    if __name__ == '__main__':
        plan_and_preprocess_entry()


setup(
    group="segmentation",
    name="nnUNetv2-plan-and-preprocess",
    version="0.1.0",
    title="nnunet plan and preprocess solution",
    description="A solution to provide the nnunet environment",
    solution_creators=["Jan Philipp Albrecht", "Maximilian Otto"],
    cite=[{
        "text": "Isensee, F., Jaeger, P. F., Kohl, S. A., Petersen, J., & Maier-Hein, K. H. (2021). nnU-Net: a self-configuring method for deep learning-based biomedical image segmentation. Nature methods, 18(2), 203-211.",
        "doi": "10.1038/s41592-020-01008-z"
    }],
    tags=["unet", "machine_learning", "images"],
    license="Apache v2",
    documentation=["https://github.com/MIC-DKFZ/nnUNet"],
    covers=[],
    album_api_version="0.5.5",
    args=
    [
        {
            "name": "data",
            "type": "string",
            "required": True,
            "description": "[REQUIRED] Path to the folder containing the raw data. This folder must contain a subfolder for each dataset. Each dataset folder must contain a folder \"imagesTr\" with the training images and a folder \"labelsTr\" with the corresponding training labels. The test images must be in a folder \"imagesTs\"."
        },
        {
            "name": "preprocessed",
            "type": "string",
            "required": True,
            "description": "[REQUIRED] Path to the folder contain or will contain the preprocessed data."
        },
        {
            "name": "id",
            "type": "string",
            "required": True,
            "description": "[REQUIRED] List of dataset IDs as string. The IDs can be up to three digit integers, contained in the data set folder name. Example: \"2 4 5\". This will run fingerprint extraction, experiment planning and preprocessing for these datasets. Can of course also be just one dataset"
        },
        {
            "name": "fpe",
            "type": "string",
            "required": False,
            "default": 'DatasetFingerprintExtractor',
            "description": '[OPTIONAL] Name of the Dataset Fingerprint Extractor class that should be used. Default is \"DatasetFingerprintExtractor\".'
        },
        {
            "name": "npfp",
            "type": "integer",
            "default": 1,
            "required": False,
            "description": "[OPTIONAL] Number of processes used for fingerprint extraction. Default: 8"
        },
        {
            "name": "verify_dataset_integrity",
            "type": "boolean",
            "required": False,
            "default": True,
            "description": "[RECOMMENDED] set this flag to check the dataset integrity. This is useful and should be done once for each dataset!"
        },
        {
            "name": "no_pp",
            "default": False,
            "required": False,
            "description": "[OPTIONAL] Set this to only run fingerprint extraction and experiment planning (no preprocesing). Useful for debugging."
        },
        {
            "name": "clean",
            "required": False,
            "default": False,
            "description": "[OPTIONAL] Set this flag to overwrite existing fingerprints. If this flag is not set and a fingerprint already exists, the fingerprint extractor will not run. REQUIRED IF YOU CHANGE THE DATASET FINGERPRINT EXTRACTOR OR MAKE CHANGES TO THE DATASET!"
        },
        {
            "name": "pl",
            "type": "string",
            "default": "ExperimentPlanner",
            "required": False,
            "description": "[OPTIONAL] Name of the Experiment Planner class that should be used. Default is \"ExperimentPlanner\". Note: There is no longer a distinction between 2d and 3d planner. Its an all in one solution now. Wuch. Such amazing."
        },
        {
            "name": "gpu_memory_target",
            "type": "integer",
            "required": False,
            "default": 8,
            "description": "[OPTIONAL] DANGER ZONE! Sets a custom GPU memory target. Default: 8 [GB]. Changing this will affect patch and batch size and will definitely affect your models performance! Only use this if you really know what you are doing and NEVER use this without running the default nnU-Net first (as a baseline)."
        },
        {
            "name": "preprocessor_name",
            "type": "string",
            "default": "DefaultPreprocessor",
            "required": False,
            "description": "[OPTIONAL] DANGER ZONE! Sets a custom preprocessor class. This class must be located in nnunetv2.preprocessing. Default: \"DefaultPreprocessor\". Changing this may affect your models performance! Only use this if you really know what you are doing and NEVER use this without running the default nnU-Net first (as a baseline)."
        },
        {
            "name": "overwrite_target_spacing",
            "default": None,
            "required": False,
            "description": "[OPTIONAL] DANGER ZONE! Sets a custom target spacing for the 3d_fullres and 3d_cascade_fullres configurations. Default: None [no changes]. Changing this will affect image size and potentially patch and batch size. This will definitely affect your models performance! Only use this if you really know what you are doing and NEVER use this without running the default nnU-Net first (as a baseline). Changing the target spacing for the other configurations is currently not implemented. New target spacing must be a list of three numbers!"
        },
        {
            "name": "overwrite_plans_name",
            "default": None,
            "required": False,
            "description": "[OPTIONAL] USE A CUSTOM PLANS IDENTIFIER. If you used -gpu_memory_target, -preprocessor_name or -overwrite_target_spacing it is best practice to use -overwrite_plans_name to generate a differently named plans file such that the nnunet default plans are not overwritten. You will then need to specify your custom plans file with -p whenever running other nnunet commands (training, inference etc)"
        },
        {
            "name": "c",
            "required": False,
            "default": "2d,3d_fullres,3d_lowres",
            "type": "string",
            "description": "[OPTIONAL] Configurations for which the preprocessing should be run. Default: 2d 3f_fullres 3d_lowres. 3d_cascade_fullres does not need to be specified because it uses the data from 3f_fullres. Configurations that do not exist for some dataset will be skipped."
        },
        {
            "name": "np",
            "type": "string",
            "default": "8,4,8",
            "required": False,
            "description": "[OPTIONAL] Use this to define how many processes are to be used. If this is just one number then this number of processes is used for all configurations specified with -c. If its a list of numbers this list must have as many elements as there are configurations. We then iterate over zip(configs, num_processes) to determine then umber of processes used for each configuration. More processes is always faster (up to the number of threads your PC can support, so 8 for a 4 core CPU with hyperthreading. If you dont know what that is then dont touch it, or at least dont increase it!). DANGER: More often than not the number of processes that can be used is limited by the amount of RAM available. Image resampling takes up a lot of RAM. MONITOR RAM USAGE AND DECREASE -np IF YOUR RAM FILLS UP TOO MUCH!. Default: 8 processes for 2d, 4 or 3d_fullres, 8 for 3d_lowres and 4 for everything else"
        },
        {
            "name": "verbose",
            "required": False,
            "default": False,
            "description": "Set this to print a lot of stuff. Useful for debugging. Will disable progress bar! Recommended for cluster environments"
        }
    ],
    run=run,
    dependencies={
        "parent": {
            "group": "segmentation",
            "name": "nnUNetv2-parent",
            "version": "0.1.0"
        },
    }
)
