# Particle Segmentation using ParticleSeg3D
## Introduction
This is a solution to segment particles in a given image of tissue samples using the [ParticleSeg3D](https://arxiv.org/pdf/2301.13319.pdf) library and its pretrained model.  
It is suitable to segment particles in micro CT scans.  
If you want to train your own model, please refer to training section of [ParticleSeg3D](segmentation:particleSeg3D-parent:0.1.0). To simply the usage of nnUNet, you can also use our corresponding solution for nnUNetv2.

The solution consists of two parts:
1. ParticleSeg3D-parent: This solution is used to install the ParticleSeg3D library and its dependencies.  
2. ParticleSeg3D-predict: This solution is used to segment particles in a given image.  

## Installation
Make sure album is already installed. If not, download and install it as described [here](https://album.solutions/).  
Also, don't forget to add the catalog to your album installation, so you can install the solutions from the catalog.

Afterwards, install the `ParticleSeg3D-predict` solution by using the graphical user interface (GUI) of album or by running the following command in the terminal:  
```bash
album install segmentation:ParticleSeg3D-predict:0.1.0
```

## How to use
<details>
  <summary><h2>ParticleSeg3D-parent</h2></summary>
This solution gets automatically installed to handle the ParticleSeg3D library and its dependencies in a separate environment.
</details>

<details open>
  <summary><h2>ParticleSeg3D-predict</h2></summary>
This solution is used to perform the instance segmentation.  
To segment images, ZARR or NIFTI files are expected. Also a `metadata.json` file (containing the voxel size and spacing) needs to exist next to the `images` folder.  
Note that if you provide NIFTI files, they get automatically converted to ZARR files.

Example folder structure: 
```bash
test
├── metadata.json
├── images
│   ├── Ore1_Zone3_Concentrate.zarr
│   ├── Recycling1.zarr
│   ├── Ore2_PS850_VS10.zarr
│   ├── Ore5.zarr
│   └── ...
└── model 
    └── Task310_particle_seg
```

The parameters can be set and run using either the GUI, or by adapting this example for command line usage:
```bash
conda activate album
album run segmentation:particleSeg3D-predict:0.1.0 --input "/data/ps3d/test/images" --output "/data/ps3d/test/test_results" --model "/data/ps3d/test/model"
```
</details>

### Further documentation: 
For further options, parameters and default values, please refer to the info page of the solution:
```bash
album info particleSegmentation-predict
```

## Hardware requirements
We recommend to use a GPU with at least 8GB of memory to run the solution as a minimum requirement.

## Citation & License
This solution is licensed under the Apache v2 License.

If you use this solution, please cite the following paper:
```
title: [Work in progress] Scalable, out-of-the box segmentation of individual particles from mineral samples acquired with micro CT,
doi: 10.48550/arXiv.2301.13319
```
