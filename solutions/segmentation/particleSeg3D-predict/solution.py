from album.runner.api import get_args, setup

def run(): 
    import os   
    os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

    import requests
    import warnings
    from pathlib import Path
    from zipfile import ZipFile
    from tqdm import tqdm
    from particleseg3d.inference.inference import setup_model, predict_cases

    args = get_args()

    if os.name == 'nt':
        print("Windows detected. Setting number of processes to `0`.")
        args.processes = 0

    def get_model(model_path: Path) -> str:
        """Downloads the model into the given path if the path to the model folder is empty or does not exist.
        Afterwards unpacks the model.

        Args:
            model_path: Path to the model folder.
        
        Returns:
            Path to the model folder.
        """
        model_path.mkdir(parents=True, exist_ok=True)
        if not any(model_path.iterdir()):
            print("No model found. Downloading the pretrained ParticleSeg3D model...")
            url = "https://syncandshare.desy.de/index.php/s/id9D9pkATrFw65s/download/Task310_particle_seg.zip"
            response = requests.get(url, stream=True)
            # to update the download progress bar, we need to know the total size of the file and iterate over the response in chunks
            total_size_in_bytes = int(response.headers.get('content-length', 0))
            with open(model_path / "Task310_particle_seg.zip", "wb") as f, tqdm(total=total_size_in_bytes, 
                                                                                unit='iB', unit_scale=True, 
                                                                                unit_divisor=1024, desc="Downloading Model", 
                                                                                colour="green") as prog_bar:
                for data in response.iter_content(chunk_size=1024):
                    f.write(data)
                    prog_bar.update(len(data))

            # UnZip the model
            with ZipFile(model_path / "Task310_particle_seg.zip", "r") as zip_ref:
                zip_ref.extractall(model_path)
            (model_path / "Task310_particle_seg.zip").unlink()
            return str(model_path / "Task310_particle_seg")
        return str(model_path)

    model_path = get_model(Path(args.model))

    images_path = Path(args.input) / "images"
    if not images_path.exists():
        raise ValueError("The given input path does not exist.")

    if Path(args.output).exists() and any(Path(args.output).iterdir()):
        warnings.warn("The given output path is not empty. This may lead to overwriting existing files.")

    if any(images_path.glob("*.nii*")) and not any(images_path.glob("*.zarr")):
        from particleseg3d.conversion.nifti2zarr import nifti2zarr
        for nii in tqdm(Path(images_path).glob("*.nii*"), desc="Converting Nifti to Zarr"):
            nifti2zarr(nii, Path(images_path) / (nii.stem + ".zarr"))

    z_score = tuple(float(num) for num in args.zscore.split())

    trainer, model, config = setup_model(model_path, args.fold.split())
    predict_cases(args.input, args.output, args.name.split(), trainer, model, config, args.target_particle_size, args.target_spacing, args.batch_size, args.processes, args.min_rel_particle_size, z_score)

    if args.save_tiff:
        from particleseg3d.conversion.zarr2tiff import zarr2tiff
        for dir in tqdm(Path(args.output).iterdir(), desc="Converting Zarr to Tiff"):
                for zarr in dir.glob("*.zarr"):
                    zarr2tiff(zarr, Path(args.output))


setup(
    group="segmentation",
    name="particleSeg3D-predict",
    version="0.1.0",
    title="particleSeg3D base solution",
    description="A solution to provide the particleSeg3D environment",
    solution_creators=["Jan Philipp Albrecht", "Maximilian Otto"],
    cite=[{
        "text": "[Work in progress] Scalable, out-of-the box segmentation of individual particles from mineral samples acquired with micro CT, Karol Gotkowski and Shuvam Gupta and Jose R. A. Godinho and Camila G. S. Tochtrop and Klaus H. Maier-Hein and Fabian Isensee, 2023",
        "doi": "tbd"
    }],
    tags=["unet", "machine_learning", "images", "segmentation", "particleSeg3D", "3D"],
    license="Apache v2",
    documentation=["https://github.com/MIC-DKFZ/ParticleSeg3D/tree/main"],
    covers=[],
    album_api_version="0.5.5",
    args=[
        {
            "name": "input",
            "description": "Absolute input path to the base folder that contains the dataset structured in the form of the directories `images` and the metadata.json. In the `images` folder, ZARR of NIFTI data is expected.",
            "type": "string",
            "required": True
        },
        {
            "name": "output",
            "description": "Absolute output path of the save folder.",
            "type": "string",
            "required": True
        },
        {
            "name": "model",
            "description": "Absolute path to the directory where the model folder is stored in. If there is no folder with a model found, the pretrained model from ParticleSeg3D gets downloaded. Example: /path/to/model/Task310_particle_seg",
            "type": "string",
            "required": True
        },
        {
            "name": "name",
            "description": "(Optional) The name(s) without extension (.zarr) of the image(s) that should be used for inference. Multiple names must be separated by spaces.",
            "type": "string",
            "required": False
        },
        {
            "name": "zscore",
            "description": "(Optional) The target spacing in millimeters given as three numbers separate by spaces.",
            "type": "string",
            "required": False,
            "default": "5850.29762143569 7078.294543817302",
        },
        {
            "name": "target_particle_size",
            "description": "(Optional) The target particle size in pixels given as three numbers separate by spaces.",
            "type": "integer",
            "required": False,
            "default": 60
        },
        {
            "name": "target_spacing",
            "description": "(Optional) The target spacing in millimeters given as three numbers separate by spaces.",
            "type": "float",
            "required": False,
            "default": 0.1
        },
        {
            "name": "fold",
            "description": "(Optional) The folds to use. 0, 1, 2, 3, 4 or a combination.",
            "type": "string",
            "required": False,
            "default": "0 1 2 3 4",
        },
        {
            "name": "batch_size",
            "description": "(Optional) The batch size to use during each inference iteration. A higher batch size decreases inference time, but increases the required GPU memory.",
            "type": "integer",
            "required": False,
            "default": 6
        },
        {
            "name": "processes",
            "description": "(Optional) Number of processes to use for parallel processing. Zero to disable multiprocessing.",
            "type": "integer",
            "required": False,
            "default": 6
        },
        {
            "name": "min_rel_particle_size",
            "description": "(Optional) Minimum relative particle size used for filtering.",
            "type": "float",
            "required": False,
            "default": 0.005
        },
        {
            "name": "save_tiff",
            "description": "(Optional) Whether to save the predictions as TIFF files. Default: True",
            "type": "boolean",
            "required": False,
            "default": True
        }
    ],
    run=run,
    dependencies={
        "parent": {
            "group": "segmentation",
            "name": "particleSeg3D-parent",
            "version": "0.1.0"
    }}
)