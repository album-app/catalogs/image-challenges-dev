from album.runner.api import get_args, setup


def run():
    args = get_args()
    from dynamic_network_architectures.architectures.unet import PlainConvUNet
    import torch
    from pathlib import Path
    import tifffile
    from torch import nn

    model = PlainConvUNet(
        input_channels=1,
        n_stages=6,
        features_per_stage=[min(32 * 2 ** i, 320) for i in range(6)],
        conv_op=nn.Conv3d,
        kernel_sizes=[[3, 3, 3], [3, 3, 3], [3, 3, 3], [3, 3, 3], [3, 3, 3], [3, 3, 3]],
        strides=[[1, 1, 1], [2, 2, 2], [2, 2, 2], [2, 2, 2], [2, 2, 2], [2, 2, 2]],
        n_conv_per_stage=[2, 2, 2, 2, 2, 2],
        num_classes=2,
        n_conv_per_stage_decoder=[2, 2, 2, 2, 2],
        deep_supervision=True,
        conv_bias=True,
        norm_op=nn.InstanceNorm3d,
        norm_op_kwargs={'eps': 1e-5, 'affine': True},
        dropout_op=None,
        dropout_op_kwargs=None,
        nonlin=nn.LeakyReLU,
        nonlin_kwargs={'inplace': True},
    )

    # load model dict from disk
    loaded_dict = torch.load(args.model_path, map_location=torch.device('cpu'))

    model.load_state_dict(loaded_dict['network_weights'])
    model.eval()

    # load input image
    input_img = tifffile.imread(args.input_img)

    # get basename of input image
    basename = Path(args.input_img).name

    assert input_img.ndim == 3, "Input image must be 3D"

    # numpy to torch
    input_tensor = torch.from_numpy(input_img).unsqueeze(0).unsqueeze(0).float()

    assert input_tensor.ndim == 5, "Input tensor must be 5D"
    assert list(input_tensor.shape) == [1, 1, 128, 128, 128], "Input tensor must be isotropic. Got {}".format(
        input_tensor.shape)

    out = model.forward(input_tensor)

    # predict on random tensor
    # out = model.forward(torch.rand(1, 1, 128, 128, 128))

    # torch to numpy
    out = out[0].detach().numpy()

    # keep prediction only
    out_label = out[:, 1, :, :, :] < 0.5
    print("Output shape: {}".format(out_label.shape))

    # save output image
    tifffile.imwrite(Path(args.output_folder).joinpath("out_" + basename), out_label)


setup(
    group="segmentation",
    name="nnUNetv2-plainUnetPredict",
    version="0.1.0",
    title="nnUNetv2 plain UNet predict",
    description="A solution to start the nnUNetv2 training",
    solution_creators=["Jan Philipp Albrecht"],
    tags=["unet", "machine_learning", "images"],
    license="Apache v2",
    documentation=["https://github.com/MIC-DKFZ/nnUNet"],
    covers=[],
    album_api_version="0.5.5",
    args=[
        {
            "name": "model_path",
            "type": "string",
            "required": True,
            "description": "Path to the model weights in .pth format"
        },
        {
            "name": "output_folder",
            "type": "string",
            "required": True,
            "description": "Desired output folder."
        },
        {
            "name": "input_img",
            "type": "string",
            "required": True,
            "description": "Path to the input image. TIF format is expected."
        },
    ],
    run=run,
    dependencies={
        "parent": {
            "group": "segmentation",
            "name": "nnUNetv2-parent",
            "version": "0.1.0"
        },
    }
)
