album_api_version: 0.5.5
args:
- description: '[REQUIRED] Path to the folder containing the raw data. This folder
    must contain a subfolder for each dataset. Each dataset folder must contain a
    folder "imagesTr" with the training images and a folder "labelsTr" with the corresponding
    training labels. The test images must be in a folder "imagesTs".'
  name: data
  required: true
  type: string
- description: '[REQUIRED] Path to the folder contain or will contain the preprocessed
    data.'
  name: preprocessed
  required: true
  type: string
- description: '[REQUIRED] Path to the folder that will contain the results data.'
  name: results_folder
  required: true
  type: string
- description: '[REQUIRED] Dataset name or ID to train with'
  name: dataset_name_or_id
  required: true
  type: string
- description: '[REQUIRED] Configuration that should be trained'
  name: configuration
  required: true
  type: string
- description: '[REQUIRED] Fold of the 5-fold cross-validation. Should be an int between
    0 and 4.'
  name: fold
  required: true
  type: string
- default: 1
  description: '[OPTIONAL] Number of threads to use for preprocessing. Default: 1'
  name: omp_num_threads
  required: false
  type: integer
- default: nnUNetTrainer
  description: '[OPTIONAL] Use this flag to specify a custom trainer. Default: nnUNetTrainer'
  name: tr
  required: false
  type: string
- default: nnUNetPlans
  description: '[OPTIONAL] Use this flag to specify a custom plans identifier. Default:
    nnUNetPlans'
  name: p
  required: false
  type: string
- default: ''
  description: '[OPTIONAL] path to nnU-Net checkpoint file to be used as pretrained
    model. Will only be used when actually training. Beta. Use with caution.'
  name: pretrained_weights
  required: false
  type: string
- default: 1
  description: Specify the number of GPUs to use for training
  name: num_gpus
  required: false
  type: integer
- default: false
  description: '[OPTIONAL] If you set this flag the training cases will not be decompressed.
    Reading compressed data is much more CPU and (potentially) RAM intensive and should
    only be used if you know what you are doing'
  name: use_compressed
  required: false
  type: boolean
- default: false
  description: '[OPTIONAL] Save softmax predictions from final validation as npz files
    (in addition to predicted segmentations). Needed for finding the best ensemble.'
  name: npz
  required: false
  type: boolean
- default: false
  description: '[OPTIONAL] Continue training from latest checkpoint'
  name: c
  required: false
  type: boolean
- default: false
  description: '[OPTIONAL] Set this flag to only run the validation. Requires training
    to have finished.'
  name: val
  required: false
  type: boolean
- default: false
  description: '[OPTIONAL] Set this flag to disable checkpointing. Ideal for testing
    things out and you dont want to flood your hard drive with checkpoints.'
  name: disable_checkpointing
  required: false
  type: boolean
- default: cuda
  description: '[OPTIONAL] Use this to set the device the training should run with.
    Available options are "cuda" (GPU), "cpu" (CPU) and "mps" (Apple M1/M2). Do NOT
    use this to set which GPU ID! Use CUDA_VISIBLE_DEVICES=X nnUNetv2_train [...]
    instead! Default: cuda'
  name: device
  required: false
  type: string
- default: false
  description: '[OPTIONAL] setting this to `True` will train all configurations and
    find the best performing automatically.'
  name: train_all_find_best
  required: false
  type: boolean
changelog: null
cite:
- doi: 10.1038/s41592-020-01008-z
  text: 'Isensee, F., Jaeger, P. F., Kohl, S. A., Petersen, J., & Maier-Hein, K. H.
    (2021). nnU-Net: a self-configuring method for deep learning-based biomedical
    image segmentation. Nature methods, 18(2), 203-211.'
covers: []
description: A solution to start the nnUNetv2 training
documentation:
- https://github.com/MIC-DKFZ/nnUNet
group: segmentation
license: Apache v2
name: nnUNetv2-train
solution_creators:
- Jan Philipp Albrecht
- Lucas Rieckert
- Maximilian Otto
tags:
- unet
- machine_learning
- images
timestamp: '2023-11-28T16:56:28.986554'
title: nnUNetv2 train
version: 0.1.0
