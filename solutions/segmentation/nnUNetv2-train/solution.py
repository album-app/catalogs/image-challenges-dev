from album.runner.api import get_args, setup


def run():
    import os
    from pathlib import Path
    args = get_args()
    # set environment variables before import
    os.environ['nnUNet_raw'] = args.data
    os.environ['nnUNet_preprocessed'] = args.preprocessed
    os.environ['nnUNet_results'] = args.results_folder
    os.environ['OMP_NUM_THREADS'] = str(args.omp_num_threads)
    os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'

    from nnunetv2.run.run_training import run_training_entry
    from nnunetv2.evaluation.find_best_configuration import find_best_configuration_entry_point

    # convert args to sys.argv
    import sys
    sys.argv = [sys.argv[0]] + [
        args.dataset_name_or_id,
        args.configuration,
        args.fold,
        "-tr", args.tr,
        "-p", args.p,
        "-num_gpus", str(args.num_gpus),
        "-device", args.device,
    ]
    if args.pretrained_weights != "":
        sys.argv += ["-pretrained_weights", args.pretrained_weights]

    # flag arguments
    if args.disable_checkpointing in ["True", "true", "1"]:
        sys.argv += ["--disable_checkpointing"]

    if args.val in [True, "True", "true", "1"]:
        sys.argv += ["--val"]

    if args.c in [True, "True", "true", "1"]:
        sys.argv += ["--c"]

    if args.use_compressed in [True, "True", "true", "1"]:
        sys.argv += ["--use_compressed"]

    if args.npz in [True, "True", "true", "1"]:
        sys.argv += ["--npz"]

    if __name__ == '__main__':
        if args.train_all_find_best in [True, "True", "true", "1"]: 
            print("Starting training for all configurations...")
            if sys.argv[-1] != "--npz":
                sys.argv += ["--npz"]

            dataset_folder = list(Path(args.preprocessed).glob("*" + args.dataset_name_or_id + "*"))[0]
            plans_folders = [Path(x) for x in dataset_folder.glob("nnUNetPlans_*")]
            plans_identifiers = [x.name.split("_", 1)[1] for x in plans_folders]

            for plans_identifier in plans_identifiers:
                sys.argv[2] = plans_identifier
                print("Running " + plans_identifier + " training...")
                run_training_entry()
                sys.argv = [sys.argv[0]] + [
                    args.dataset_name_or_id,
                    args.fold,
                    "-tr", args.tr,
                    "-p", args.p,
                    "-c", plans_identifiers,
                    "--disable_ensembling"]
            print("Finding best configuration...")
            find_best_configuration_entry_point()
        else:
            print("Starting training...")
            print(sys.argv)
            run_training_entry()


setup(
    group="segmentation",
    name="nnUNetv2-train",
    version="0.1.0",
    title="nnUNetv2 train",
    description="A solution to start the nnUNetv2 training",
    solution_creators=["Jan Philipp Albrecht", "Lucas Rieckert", "Maximilian Otto"],
    cite=[{
        "text": "Isensee, F., Jaeger, P. F., Kohl, S. A., Petersen, J., & Maier-Hein, K. H. (2021). nnU-Net: a self-configuring method for deep learning-based biomedical image segmentation. Nature methods, 18(2), 203-211.",
        "doi": "10.1038/s41592-020-01008-z"
    }],
    tags=["unet", "machine_learning", "images"],
    license="Apache v2",
    documentation=["https://github.com/MIC-DKFZ/nnUNet"],
    covers=[],
    album_api_version="0.5.5",
    args=[
        {
            "name": "data",
            "type": "string",
            "required": True,
            "description": "[REQUIRED] Path to the folder containing the raw data. This folder must contain a subfolder for each dataset. Each dataset folder must contain a folder \"imagesTr\" with the training images and a folder \"labelsTr\" with the corresponding training labels. The test images must be in a folder \"imagesTs\"."
        },
        {
            "name": "preprocessed",
            "type": "string",
            "required": True,
            "description": "[REQUIRED] Path to the folder contain or will contain the preprocessed data."
        },
        {
            "name": "results_folder",
            "type": "string",
            "required": True,
            "description": "[REQUIRED] Path to the folder that will contain the results data."
        },
        {
            "name": "dataset_name_or_id",
            "type": "string",
            "required": True,
            "description": "[REQUIRED] Dataset name or ID to train with"
        },
        {
            "name": "configuration",
            "type": "string",
            "required": True,
            "description": "[REQUIRED] Configuration that should be trained"
        },
        {
            "name": "fold",
            "type": "string",
            "required": True,
            "description": '[REQUIRED] Fold of the 5-fold cross-validation. Should be an int between 0 and 4.'
        },
        {
            "name": "omp_num_threads",
            "type": "integer",
            "required": False,
            "default": 1,
            "description": "[OPTIONAL] Number of threads to use for preprocessing. Default: 1"
        },
        {
            "name": "tr",
            "type": "string",
            "required": False,
            "default": "nnUNetTrainer",
            "description": "[OPTIONAL] Use this flag to specify a custom trainer. Default: nnUNetTrainer"
        },
        {
            "name": "p",
            "type": "string",
            "required": False,
            "default": "nnUNetPlans",
            "description": "[OPTIONAL] Use this flag to specify a custom plans identifier. Default: nnUNetPlans"
        },
        {
            "name": "pretrained_weights",
            "type": "string",
            "required": False,
            "default": "",
            "description": "[OPTIONAL] path to nnU-Net checkpoint file to be used as pretrained model. Will only be used when actually training. Beta. Use with caution."
        },
        {
            "name": "num_gpus",
            "type": "integer",
            "default": 1,
            "required": False,
            "description": "Specify the number of GPUs to use for training"
        },
        {
            "name": "use_compressed",
            "default": False,
            "required": False,
            "type": "boolean",
            "description": "[OPTIONAL] If you set this flag the training cases will not be decompressed. Reading compressed data is much more CPU and (potentially) RAM intensive and should only be used if you know what you are doing"
        },
        {
            "name": "npz",
            "default": False,
            "required": False,
            "type": "boolean",
            "description": "[OPTIONAL] Save softmax predictions from final validation as npz files (in addition to predicted segmentations). Needed for finding the best ensemble."
        },
        {
            "name": "c",
            "default": False,
            "required": False,
            "type": "boolean",
            "description": "[OPTIONAL] Continue training from latest checkpoint"
        },
        {
            "name": "val",
            "default": False,
            "required": False,
            "type": "boolean",
            "description": "[OPTIONAL] Set this flag to only run the validation. Requires training to have finished."
        },
        {
            "name": "disable_checkpointing",
            "default": False,
            "required": False,
            "type": "boolean",
            "description": "[OPTIONAL] Set this flag to disable checkpointing. Ideal for testing things out and you dont want to flood your hard drive with checkpoints."
        },
        {
            "name": "device",
            "type": "string",
            "default": "cuda",
            "required": False,
            "description": "[OPTIONAL] Use this to set the device the training should run with. Available options are \"cuda\" (GPU), \"cpu\" (CPU) and \"mps\" (Apple M1/M2). Do NOT use this to set which GPU ID! Use CUDA_VISIBLE_DEVICES=X nnUNetv2_train [...] instead! Default: cuda"
        },
        {
            "name": "train_all_find_best",
            "default": False,
            "required": False,
            "type": "boolean",
            "description": "[OPTIONAL] setting this to `True` will train all configurations and find the best performing automatically."
        }
    ],
    run=run,
    dependencies={
        "parent": {
            "group": "segmentation",
            "name": "nnUNetv2-parent",
            "version": "0.1.0"
        },
    }
)
