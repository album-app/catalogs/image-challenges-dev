from album.runner.api import get_args, setup

def run():
    # parse arguments
    args = get_args()

    # imports 
    import argparse
    import numpy as np
    import tifffile
    from skimage.io import imread
    from pathlib import Path
    from deepcell.applications import Mesmer

    def segmentation(args: argparse.Namespace, image_nuclear: str, image_membrane: str = None) -> str:
        """Loads the color channels, prepares them for the network, and runs the segmentation.

        Args:
            args: Arguments provided by the user.
            image_nuclear: Name of the image containing the nuclei.
            image_membrane: Name of the image containing the membrane (optional).

        Returns:
            Name of the processed image.
        """
        image_nuc = imread(Path(args.input_path) / image_nuclear)

        if (len(image_nuc.shape) > 2):
            image_nuc = image_nuc[:,:,args.channel_nuclear]
        if (len(image_nuc.shape) == 2):
            image_nuc = np.expand_dims(image_nuc, axis=-1)

        if image_membrane:
            image_mem = imread(Path(args.input_path) / image_membrane)
            if (len(image_mem.shape) > 2):
                image_mem = image_mem[:,:,args.channel_membrane]
            if (len(image_mem.shape) == 2):
                # Expand from [H, W] to [H, W, C]
                image_mem = np.expand_dims(image_mem, axis=-1)
        else:
            # Create an empty channel for membrane if it's not provided
            image_mem = np.zeros_like(image_nuc)
            args.segmentation_mode = "nuclear"

        # Combine and expand to [1, H, W, C]
        image_prepped = np.concatenate([image_nuc, image_mem], axis=-1)
        image_prepped = np.expand_dims(image_prepped,0) 

        # Model loading
        app = Mesmer()
        print('Image resolution the network was trained on:', app.model_mpp, 'microns per pixel')

        # Postprocessing
        if args.maxima_threshold is None:
            if args.segmentation_mode == "whole-cell":
                args.maxima_threshold = 0.1
            elif args.segmentation_mode == "nuclear":
                args.maxima_threshold = 0.075

        postprocess = {
            "maxima_threshold": args.maxima_threshold,
            "maxima_smooth": args.maxima_smooth,
            "interior_threshold": args.interior_threshold,
            "interior_smooth": args.interior_smooth,
            "small_objects_threshold": args.small_objects_threshold,
            "fill_holes_threshold": args.fill_holes_threshold,
            "radius": args.radius,
            "pixel_expansion": args.pixel_expansion
        }

        # Prediction
        segmentation_predictions = app.predict(image_prepped, image_mpp=args.image_mpp, postprocess_kwargs_whole_cell=postprocess, postprocess_kwargs_nuclear=postprocess, compartment=args.segmentation_mode, batch_size=1)
        segmentation_predictions = np.squeeze(segmentation_predictions)
        # Convert from 64 to 16 bit
        segmentation_predictions = segmentation_predictions.astype(np.uint16)

        # Save the segmentation predictions
        image_name = Path(image_nuclear).stem
        output_name = Path(args.output_path) / (image_name + "_segmentation")
        Path(args.output_path).mkdir(parents=True, exist_ok=True)
        if args.save_mask:
            tifffile.imwrite(output_name.with_suffix(".tiff"), segmentation_predictions)
        if args.save_npy:
            np.save(output_name, segmentation_predictions)
        return output_name

    # Test if input_path contains tif files
    input_path = Path(args.input_path)
    if not input_path.is_dir():
        raise ValueError("The provided folder path is not a folder.")

    # Run on one sample or whole folder
    if args.img_name_nuclear:
        output_name = segmentation(args, args.img_name_nuclear, args.img_name_membrane)
    else:
        files = list(input_path.glob('*.tif*'))
        if not files:
            raise ValueError("No tif files found in folder.")
        for file in files:
            output_name = segmentation(args, file, file)

    print("Recent segmentation saved to:", output_name.resolve())
    return 

setup(
    group="segmentation",
    name="DeepCell-predict",
    version="0.1.0",
    title="DeepCell Mesmer Segmentation",
    description="A solution to create segmentations with the previously trained deepCell Mesmer model",
    solution_creators=["Maximilian Otto", "Jan Philipp Albrecht"],
    tags=["segmentation", "machine_learning", "images", "deepcell", "mesmer", "2D"],
    license="Modified Apache v2",
    documentation=["https://deepcell.readthedocs.io/en/master/"],
    cite=[{
        "text": "Noah F. Greenwald and Geneva Miller and Erick Moen and Alex Kong and Adam Kagel and Thomas Dougherty and Christine Camacho Fullaway and Brianna J. McIntosh and Ke Xuan Leow and Morgan Sarah Schwartz and Cole Pavelchek and Sunny Cui and Isabella Camplisson and Omer Bar-Tal and Jaiveer Singh and Mara Fong and Gautam Chaudhry and Zion Abraham and Jackson Moseley and Shiri Warshawsky and Erin Soon and Shirley Greenbaum and Tyler Risom and Travis Hollmann and Sean C. Bendall and Leeat Keren and William Graf and Michael Angelo and David Van Valen; Whole-cell segmentation of tissue images with human-level performance using large-scale data annotation and deep learning",
        "doi": "https://doi.org/10.1038/s41587-021-01094-0"
    }],
    covers=[{
        "description": "DeepCell Core Library Logo",
        "source": "cover.png"
    }],
    album_api_version="0.5.5",
    args=[
        {
            "name": "input_path",
            "type": "string",
            "required": True,
            "description": "Full path to the folder containing the images.",
            "default": "./"
        },
        {
            "name": "img_name_nuclear",
            "type": "string",
            "required": False,
            "description": "Name of image containing the nuclei. (If not set or empty, the solution will run on all images in the folder while assuming each image contains nuclei and membrane information in the provided color channels.)",
            "default": ""
        },
        {
            "name": "channel_nuclear",
            "type": "integer",
            "required": True,
            "description": "Channel index of the nuclear image, starting with 0.",
            "default": 0
        },
        {
            "name": "img_name_membrane",
            "type": "string",
            "required": False,
            "description": "Name of the image containing the membrane information. This can be in the same file as the nuclear image, but would require to set `channel_membrane` to a different value. If not set, an empty channel will be used for the membrane.",
            "default": ""
        },
        {
            "name": "channel_membrane",
            "type": "integer",
            "required": False,
            "description": "Channel index of the membrane image, starting with 0. If `img_name_membrane` is not set, this value will be ignored.",
            "default": 1
        },
        {
            "name": "image_mpp",
            "type": "float",
            "required": False,
            "description": "Resolution of the images in `Microns per pixel`. If not set, it will default to 0.5.",
            "default": 0.5
        },
        {
            "name": "segmentation_mode",
            "type": "string",
            "required": False,
            "description": "Segmentation mode can be either `whole-cell` or `nuclear`. If `img_name_membrane` is not set, this will default to `nuclear`.",
            "default": "whole-cell"
        },
        {
            "name": "output_path",
            "type": "string",
            "required": False,
            "description": "Full path to the output folder in which the result gets stored.",
            "default": "./output"
        },
        {
            "name": "save_mask",
            "type": "boolean",
            "required": False,
            "description": "Set this to `True` to save the segmentation mask as a tif file.",
            "default": True
        },
        {
            "name": "save_npy",
            "type": "boolean",
            "required": False,
            "description": "Set this to `True` to save the segmentation mask as a numpy file.",
            "default": False
        },
        {
            "name": "maxima_threshold",
            "type": "float",
            "required": False,
            "description": "To finetune specific and consistent errors in your data, the following arguments can be used during postprocessing. \nLower values will result in more cells being detected. Higher values will result in fewer cells being detected. Default for `whole_cell`: 0.1, default for `nuclear`: 0.075",
        },
        {
            "name": "maxima_smooth",
            "type": "float",
            "required": False,
            "description": "Default: 0",
            "default": 0.0
        },
        {
            "name": "interior_threshold",
            "type": "float",
            "required": False,
            "description": "Comparison threshold for cell vs. background. Lower values tend to result in larger cells. Default: 0.2",
            "default": 0.2
        },
        {
            "name": "interior_smooth",
            "type": "float",
            "required": False,
            "description": "Default: 0",
            "default": 0.0
        },
        {
            "name": "small_objects_threshold",
            "type": "float",
            "required": False,
            "description": "Default: 15",
            "default": 15.
        },
        {
            "name": "fill_holes_threshold",
            "type": "float",
            "required": False,
            "description": "Default: 15",
            "default": 15.
        },
        {
            "name": "radius",
            "type": "float",
            "required": False,
            "description": ". Default: 2",
            "default": 2.
        }, 
        {
            "name": "pixel_expansion",
            "type": "integer",
            "required": False,
            "description": "Add a manual pixel expansion after segmentation to each cell. Default: 0",
            "default": 0
        }
    ],
    run=run,
    dependencies={
        "parent": {
                    "group": "segmentation",
                    "name": "DeepCell-tf-parent",
                    "version": "0.1.0",
                    },        
    }
)
