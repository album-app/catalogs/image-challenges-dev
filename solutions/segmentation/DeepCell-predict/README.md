# Cell Segmentation using DeepCell 

## Introduction
This is a solution to segment cells in a given image of tissue samples using the [DeepCell](https://www.nature.com/articles/s41587-021-01094-0) library.  
It is suitable for images of tissue samples with stained nuclei and membranes or cytoplasm (e.g. DAPI and E-Cadherin).  
This allows to apply the pre-existing Mesmer-segmentation model of DeepCell locally for high throughput, meaning that you can segment large amounts of images at once.   

The solution consists of two parts:
1. DeepCell-parent: This solution is used to install the DeepCell library and its dependencies.
2. DeepCell-predict: This solution is used to segment cells in a given image.

<details>
  <summary><h2>Installation</h2></summary>
Make sure album is already installed. If not, download and install it as described [here](https://album.solutions/). Also, don't forget to add the catalog to your album installation, so you can install the solutions from the catalog.  

Afterwards, install the `DeepCell-predict` solution in the same manner using the graphical user interface (GUI) of album or by running the following command in the terminal:  
```bash
album install segmentation:DeepCell-predict:0.1.0
```
</details>

## How to use
<details>
  <summary><h2>DeepCell-parent</h2></summary>
    This solution gets automatically installed to handle the DeepCell-tf library and its dependencies in a separate environment.
</details>

<details open>
  <summry><h2>DeepCell-predict</h2></summary>

This solution is used to perform the segmentation.    
To segment just **one sample**, provide two greyscale `TIF` files:
- one for the nuclei
- one for the cytoplasm or membrane (Optional for whole-cell segmentation)  

If you have an image containing both channels, you can pass the same file twice and define the corresponding channel index, using either the GUI, or by adapting this example for command line usage:
```bash
album run DeepCell-predict --input_path "/dataPath/images" --img_name_nuclear "myTissueSample.tif" --channel_nuclear 0 --img_name_membrane "myTissueSample.tif" --channel_membrane 1
```

To segment **all files from a folder** at once, just provide the path to the folder and which information can be found at which color channel index. Each image must contain at least a nuclei and membrane(or cytoplasm) channel to perform whole-cell segmentation.  
The parameters can be set using either the GUI, or by adapting this example for command line usage:
```bash
album run DeepCell-predict --input_path "/dataPath/folderToSegment" --channel_nuclear 0 --channel_membrane 1
```

For further options and default parameters, please refer to the info page of the solution:
```bash
album info DeepCell-predict
```
</details>

## Citation & License
This solution is licensed under a modified Apache v2 License. See [LICENSE](https://github.com/vanvalenlab/deepcell-tf/blob/master/LICENSE) for the full text.

If you use this solution, please cite the following paper:
```
  doi: 10.1038/s41587-021-01094-0
  title: Whole-cell segmentation of tissue images with human-level performance using large-scale data annotation and deep learning
```
