# Instance Segmentation using nnU-Net V2
## Introduction
This is a collection of solutions to train and execute a U-Net model using nnU-Net V2 to perform instance segmantation in any set of images.   

The package will be listed in album's catalog as four parts:
1. parent
2. plan & preprocess
3. train
4. predict

## Installation
Make sure album is already installed. If not, download and install it as described [here](https://album.solutions/).  
Also, don't forget to add the catalog to your album installation, so you can install the solutions from the catalog.  

Afterwards, install the solutions in the same manner using the graphical user interface (GUI) of album or by running the following commands in the terminal:  
```bash
album install segmentation:nnUNetv2-prepare-data:0.1.0
album install segmentation:nnUNetv2-plan-and-preprocess:0.1.0
album install segmentation:nnUNetv2-train:0.1.0
album install segmentation:nnUNetv2-predict:0.1.0
album install segmentation:nnUNetv2-plainUnetPredict:0.1.0
```

## How to use
<details>
  <summary><h2>nnUNetv2-parent</h2></summary>
This solution gets automatically installed to handle the nnUNetv2 library and its dependencies in a separate environment.
</details>

<details open>
  <summary><h2>nnUNetv2-prepare-data</h2></summary>
Before you can train and predict with the nnUNetv2 solutions, you need to prepare the data. This solution takes the raw data and converts it into the format required by nnUNetv2.  
nnUNetv2 requires the data to be named and stored in a specific way. The data needs to be stored in a directory named `nnUNet_raw` and the images need to be stored in a subdirectory named `imagesTr` and the corresponding labels in `labelsTr`.  
We recommend to use the `nnUNetv2-prepare-data` solution to prepare the data. It takes the raw data and converts it into the required format.  
</details>

Please have a look at the official [nnUNetv2 documentation](https://github.com/MIC-DKFZ/nnUNet/blob/master/documentation/dataset_format.md#dataset-folder-structure) for further information on the folder structure and examples.

<details>
  <summary><h2>nnUNetv2-plan-and-preprocess</h2></summary>
The nnUNetv2 solutions require three parameters to set up the environment:
- `data`: The path to the raw data directory.
- `preprocessed`: Location of the directory for the (then) preprocessed data.
- `results_folder`: Path to the directory to store the final segmentations in.  

To prepare the data for training and inference, you can use this solution. It also stores the different network configurations that can be used in the next steps. Usually, this involves low and high resultion networks for 2D and 3D segmentation.  

The parameters can be set and run using either the GUI, or by adapting this example for command line usage:
```bash
conda activate album
album run nnUNetv2-plan-and-preprocess --data "/data/nnUNet/nnUNet_raw" --preprocessed "/data/nnUNet/nnUNet_preprocessed" --id 999"
```
</details>

<details>
  <summary><h2>nnUNetv2-train</h2></summary> 
To train a U-Net configuration on the previously preprocessed images, state the data set ID, specific configuration (2d, 3d, etc.) and folds you want to train on.  

```bash 
album run nnUNetv2-train --data "/data/nnUNet/nnUNet_raw" --preprocessed "/data/nnUNet/nnUNet_preprocessed" --results_folder "/data/nnUNet/nnUNet_results" --dataset_name_or_id 999 --configuration 2d --fold 0
```
</details>

<details>
  <summary><h2>nnUNetv2-predict</h2></summary>
To perform the instance segmentation, you can use the `nnUNetv2-predict` solution. It takes the preprocessed data and the trained model and performs the segmentation. State the 

```bash
album run nnUNetv2-predict --data "/data/nnUNet/nnUNet_raw" --preprocessed "/data/nnUNet/nnUNet_preprocessed" --results_folder "/data/nnUNet/nnUNet_results" --input_path "data/predict_this" --output_folder "data/nnUNet/predictions" --dataset_name_or_id 999 --configuration 2d --fold 0
```
</details>

### Further documentation: 
For further options, parameters and default values, please refer to the info pages of the corresponding solutions:
```bash
album info <specific_solution_name>
```

# Hardware requirements
The smallest setup we used to run the solutions consisted of 32GB of RAM and a GPU with 8GB of VRAM, so we define these as minimal requirements, but recommend a GPU with 10GB VRAM and a CPU with 12 or more threads according to the official [documentation](https://github.com/MIC-DKFZ/nnUNet/blob/master/documentation/installation_instructions.md#hardware-requirements-for-training). 

## Citation & License
This solution is licensed under the Apache v2 License.

If you use this solution, please cite the following paper:
```
doi: 10.1038/s41592-020-01008-z,
title: nnU-Net: a self-configuring method for deep learning-based biomedical image segmentation
```
