from album.runner.api import get_args, setup

env_file = """
name: prepare-nnUNetv2-data
channels:
    - conda-forge
    - defaults
dependencies:
    - python=3.8
    - pip
    - pip:
      - tifffile
      - numpy
"""


def run():
    args = get_args()

    from pathlib import Path
    import os
    import tifffile
    import numpy as np
    import json

    # get base arguments
    in_path = Path(args.root)
    out_path = Path(args.out)
    ds_name = args.ds_name
    channel_name = args.channel_name
    labels_name = args.label_name

    assert ds_name.startswith("Dataset"), "Improper dataset naming schema. See description for help!"
    assert not channel_name.isspace(), "Channel name must not contain whitespaces!"
    assert not labels_name.isspace(), "Labels name must not contain whitespaces!"

    ds_name_prefix = ds_name.split('_')[0].replace('Dataset', '')
    ds_name_suffix = "_".join(ds_name.split('_')[1:])

    # check for whitespaces
    assert not ds_name_suffix.isspace(), "Dataset name prefix must not contain whitespaces!"

    print(f"Your dataset ID is: {ds_name_prefix}")

    # prepare relative path for images and masks os independent
    images_relative_path_ = args.images.split("/")
    masks_relative_path_ = args.masks.split("/")

    images_relative_path = in_path.joinpath(*images_relative_path_)
    masks_relative_path = in_path.joinpath(*masks_relative_path_)

    # list all images and masks
    imgs_list_sorted = sorted(os.listdir(in_path.joinpath(images_relative_path)))
    masks_list_sorted = sorted(os.listdir(in_path.joinpath(masks_relative_path)))

    # remove hidden files
    imgs_list_sorted = [x for x in imgs_list_sorted if not str(x).startswith('.')]
    masks_list_sorted = [x for x in masks_list_sorted if not str(x).startswith('.')]

    # check for same length
    assert len(imgs_list_sorted) == len(masks_list_sorted), \
        f"Number of images {len(imgs_list_sorted)} and masks {len(masks_list_sorted)} is not equal!"

    # check for non-tif files
    for img in imgs_list_sorted:
        assert str(img).endswith(".tif"), "Images must be in tif format!"
    for mask in masks_list_sorted:
        assert str(mask).endswith(".tif"), "Masks must be in tif format!"

    # create output folders
    out_path.mkdir(parents=True, exist_ok=True)
    out_path.joinpath(ds_name).mkdir(parents=True, exist_ok=True)

    img_out = out_path.joinpath(ds_name).joinpath("imagesTr")
    mask_out = out_path.joinpath(ds_name).joinpath("labelsTr")

    img_out.mkdir(parents=True, exist_ok=True)
    mask_out.mkdir(parents=True, exist_ok=True)

    # copy the images
    for idx, (img, mask) in enumerate(zip(imgs_list_sorted, masks_list_sorted)):
        abs_path_img = in_path.joinpath(images_relative_path).joinpath(str(img))
        abs_path_mask = in_path.joinpath(masks_relative_path).joinpath(str(mask))

        # check for one channel and mask
        np_img = tifffile.imread(abs_path_img)
        np_mask = tifffile.imread(abs_path_mask)
        assert len(np.squeeze(np_img).shape) == 3, "Images must be single channel 3D image!"
        assert len(np.unique(np_mask)) <= 2, \
            "Masks must be binary! Found %s unique values." % str(len(np.unique(np_mask)))

        # correct label offset (e.g. assure consecutive labels)
        if np_mask.max() > 0:
            np_mask[np_mask == np_mask.max()] = 1

        # copy the images
        new_name_image = f"{ds_name_suffix}_{idx:03d}_0000.tif"
        new_name_mask = f"{ds_name_suffix}_{idx:03d}.tif"

        tifffile.imwrite(img_out.joinpath(new_name_image), np_img)
        tifffile.imwrite(mask_out.joinpath(new_name_mask), np_mask)

    # build the dataset.json
    dataset_json = {
        "channel_names": {"0": channel_name},
        "labels": {"background": 0, labels_name: 1},
        "numTraining": len(imgs_list_sorted),
        "file_ending": ".tif"
    }

    with open(out_path.joinpath(ds_name).joinpath("dataset.json"), 'w') as f:
        json.dump(dataset_json, f, indent=4)


setup(
    group="segmentation",
    name="cellsketch-prepare-nnUNetv2-data",
    version="0.1.0",
    title="CellSketch: Prepare data for nnUNetv2",
    description="A solution to provide the nnunet environment",
    solution_creators=["Jan Philipp Albrecht"],
    tags=["unet", "machine_learning", "images"],
    license="Apache v2",
    documentation=["https://github.com/MIC-DKFZ/nnUNet"],
    covers=[],
    album_api_version="0.5.5",
    args=
    [
        {
            "name": "root",
            "type": "string",
            "required": True,
            "description": "The root path to the data folder."
        },
        {
            "name": "out",
            "type": "string",
            "required": True,
            "description": "The path to the output folder."
        },
        {
            "name": "images",
            "type": "string",
            "default": "train/images/",
            "required": False,
            "description": "The path to the images folder relative from the root. Must contain the images in tif format."
        },
        {
            "name": "masks",
            "type": "string",
            "default": "train/masks/",
            "required": False,
            "description": "The path to the masks folder relative from the root. Must contain the masks in tif format."
        },
        {
            "name": "ds_name",
            "type": "string",
            "default": "Dataset999_3DFIBSEM_GOLGI",
            "required": False,
            "description": "The name of the dataset. Format is \"DatasetXXX_Y\", where XXX is a three digit number and Y is an arbitrary name without whitespaces."
        },
        {
            "name": "channel_name",
            "type": "string",
            "default": "FIBSEM",
            "required": False,
            "description": "The name of the channel. E.g. what is the origin of the data?"
        },
        {
            "name": "label_name",
            "type": "string",
            "default": "GOLGI",
            "required": False,
            "description": "The name of the label. E.g. what is the object of interest?"
        }
    ],
    run=run,
    dependencies={"environment_file": env_file}
)
