from album.runner.api import setup

env="""name:  cellpose_predict
channels:
  - pytorch
  - nvidia
  - conda-forge
dependencies:
  - python=3.10
  - tifffile>=2023.7.18
  - pytorch-cuda==12.1
  - pytorch>=2.0.0
  - pip
  - pip:
    - cellpose==3.0.6
    - cellpose[gui]==3.0.6
"""


def run():
    import runpy
    # start module and end solution when its window is closed
    runpy.run_module("cellpose", run_name="__main__")
    return


setup(
    group="segmentation",
    name="cellpose_interactive",
    version="0.1.0",
    title="Cellpose Interactive",
    description="Installs and launches the Cellpose GUI for interactive segmentation.",
    solution_creators=["Maximilian Otto"],
    cite=[{"text": "Cellpose 2.0: how to train your own model", "doi": "10.1038/s41592-022-01663-4"}, 
          {"text": "Cellpose: a generalist algorithm for cellular segmentation", "doi": "10.1038/s41592-020-01018-x"}],
    tags=["segmentation", "cellpose", "interactive", "gui", "machine learning", "deep learning", "cells", "nuclei", "cytoplasm"],
    license="BSD-3-Clause",
    album_api_version="0.5.5",
    covers=[],
    documentation=[],
    args=[],
    dependencies={"environment_file": env},
    run=run
)
