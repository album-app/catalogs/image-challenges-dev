from album.runner.api import setup

env_file = """
channels:
  - conda-forge
dependencies:
  - python=3.9
  - tifffile
  - zarr
  - ome-zarr
  - numpy
  - scikit-image
  - dask
"""

def install():
    print("Installing this solution...")

def run():
    # Import necessary modules
    from album.runner.api import get_args
    import os
    import tifffile
    import zarr
    from ome_zarr.io import parse_url
    from ome_zarr.writer import write_multiscale
    import numpy as np
    from dask.array import from_array, coarsen
    from dask.diagnostics import ProgressBar

    # Get arguments
    args = get_args()
    tiff_dir = args.tiff_dir
    tmp_dir = args.tmp_dir
    output_dir = args.output_dir
    dataset = args.dataset
    channel = args.channel

    # Parse comma-separated strings into lists
    transform_scale = [float(x) for x in args.transform_scale.split(',')]
    chunk_size = tuple(int(x) for x in args.chunk_size.split(','))
    factors = [int(x) for x in args.factors.split(',')]
    num_levels = int(args.num_levels)

    # Construct paths
    tiff_directory = os.path.join(tiff_dir)
    output_path = os.path.join(tmp_dir)
    output_path2 = os.path.join(output_dir)

    # Ensure output directories exist
    os.makedirs(os.path.dirname(output_path), exist_ok=True)
    os.makedirs(os.path.dirname(output_path2), exist_ok=True)

    # List all TIFF files and sort them
    tiff_files = sorted([f for f in os.listdir(tiff_directory) if f.endswith('.tif')])

    if not tiff_files:
        raise FileNotFoundError(f"No TIFF files found in directory {tiff_directory}")

    # Read the first image to get the dimensions and data type
    first_image = tifffile.imread(os.path.join(tiff_directory, tiff_files[0]))
    height, width = first_image.shape
    num_slices = len(tiff_files)
    data_type = first_image.dtype

    # Define the coarsening factors
    total_factor = [f ** num_levels for f in factors]

    def calculate_padding(shape, total_factor):
        pad = [0, 0, 0]
        for i in range(3):
            if shape[i] % total_factor[i] != 0:
                pad[i] = total_factor[i] - (shape[i] % total_factor[i])
            print(f"Dimension {i}: total_factor={total_factor[i]}, pad[{i}]={pad[i]}")
        return tuple(pad)

    # Calculate the padding needed for each dimension
    padding = calculate_padding((num_slices, height, width), total_factor)
    padded_shape = (num_slices + padding[0], height + padding[1], width + padding[2])

    print(f"Padded Shape: {padded_shape}")
    print(f"Padding: {padding}")

    # Create the zarr array
    store = zarr.DirectoryStore(output_path)
    z = zarr.creation.open_array(store=store, mode='w', shape=padded_shape, chunks=chunk_size, dtype=data_type)

    # Process images in batches
    batch_size = 128  # Process 128 slices at a time
    for i in range(0, num_slices, batch_size):
        print(f"Processing slices {i} to {min(i + batch_size - 1, num_slices - 1)}")
        batch_end = min(i + batch_size, num_slices)
        batch_data = np.zeros((batch_end - i, height, width), dtype=data_type)

        for j, filename in enumerate(tiff_files[i:batch_end]):
            img = tifffile.imread(os.path.join(tiff_directory, filename))
            batch_data[j, :, :] = img

        z[i:batch_end, :height, :width] = batch_data

    # Now create multiscale data
    # Re-open the zarr array
    z = zarr.open_array(store=store, mode='r')

    # Rechunk the data before creating multiscale data
    d0 = from_array(z, chunks=chunk_size)

    # Create multiscale data
    scales = [d0]

    def mean_dtype(arr, **kwargs):
        return np.mean(arr, **kwargs).astype(arr.dtype)

    def downscale_block(data, factors):
        coarsening_factors = {i: f for i, f in enumerate(factors)}
        return coarsen(mean_dtype, data, coarsening_factors)

    for level in range(num_levels):
        current_shape = scales[-1].shape
        print(f"Current shape before downscaling at level {level + 1}: {current_shape}")
        if all(s // f > 0 for s, f in zip(current_shape, factors)):
            downscaled = downscale_block(scales[-1], factors)
            scales.append(downscaled)
            print(f"Shape after downscaling at level {level + 1}: {downscaled.shape}")
        else:
            print(f"Stopping coarsening as factors do not align with shape {current_shape}")
            break

    # Ensure each scale has different shapes
    for i, scale in enumerate(scales):
        print(f"Scale {i} shape: {scale.shape}")

    # Write the OME-ZARR data
    store = parse_url(output_path2, mode='w').store
    root = zarr.group(store=store)
    # Write OME-ZARR metadata
    axes = [
        {'name': 'z', 'type': 'space', 'unit': 'micrometer'},
        {'name': 'y', 'type': 'space', 'unit': 'micrometer'},
        {'name': 'x', 'type': 'space', 'unit': 'micrometer'}
    ]
    coordinate_transformations = []
    for i, scale in enumerate(scales):
        scale_factors = tuple(
            (transform_scale[dim] * scales[0].shape[dim] / scale.shape[dim]) for dim in range(3)
        )
        coordinate_transformations.append([
            {'type': 'scale', 'scale': list(scale_factors)}
        ])

    # Write multiscale data
    with ProgressBar():
        write_multiscale(
            [s.astype(data_type) for s in scales],
            root,
            coordinate_transformations=coordinate_transformations,
            axes=axes,
            name=f"{dataset} {channel}"
        )


setup(
    group="conversion",
    name="tiff-stack-to-ome-zarr",
    version="0.1.0-SNAPSHOT",
    album_api_version="0.5.5",
    title="Converting TIFF slices into multiresolution OME-ZARR",
    description="This solution converts a 3D dataset consisting of 2D TIFF slices into a multiresolution OME-ZARR dataset.",
    solution_creators=["Your Name"],
    cite=[{
        "text": "Your citation text",
        "doi": "Your DOI"
    }],
    tags=["image processing", "ome-zarr", "tiff"],
    license="MIT",
    documentation="",
    covers=[{
        "description": "Cover image",
        "source": "cover.png"
    }],
    args=[
        {
            "name": "tiff_dir",
            "type": "string",
            "description": "Directory containing the TIFF files.",
            "required": True
        },
        {
            "name": "tmp_dir",
            "type": "string",
            "description": "Temporary directory for intermediate files.",
            "required": True
        },
        {
            "name": "output_dir",
            "type": "string",
            "description": "Output directory for the OME-ZARR dataset.",
            "required": True
        },
        {
            "name": "dataset",
            "type": "string",
            "description": "Dataset name.",
            "required": True
        },
        {
            "name": "channel",
            "type": "string",
            "description": "Channel name.",
            "required": True
        },
        {
            "name": "transform_scale",
            "type": "string",
            "default": "4.0,1.8,1.8",
            "description": "Transformation scale as comma-separated floats (e.g., 4.0,1.8,1.8).",
        },
        {
            "name": "chunk_size",
            "type": "string",
            "default": "64,256,256",
            "description": "Chunk size as comma-separated integers (e.g., 64,256,256).",
        },
        {
            "name": "factors",
            "type": "string",
            "default": "2,2,2",
            "description": "Downsampling factors as comma-separated integers (e.g., 2,2,2).",
        },
        {
            "name": "num_levels",
            "type": "string",
            "default": "7",
            "description": "Number of downsampled levels.",
        }
    ],
    run=run,
    dependencies={'environment_file': env_file}
)
