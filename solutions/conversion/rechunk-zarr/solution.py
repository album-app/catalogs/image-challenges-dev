from album.runner.api import setup

env_file = """
channels:
  - conda-forge
dependencies:
  - python=3.11
  - zarr
  - rechunker
"""


def run():
    from album.runner.api import get_args
    import zarr
    from rechunker import rechunk

    # Get arguments
    input_zarr = get_args().input_zarr
    output_zarr = get_args().output_zarr

    source_group = zarr.open(input_zarr)
    print(source_group.info)

    if not output_zarr:
        print("No output ZARR provided, nothing is chunked.")
        return

    tmp_zarr = get_args().tmp_zarr
    new_chunk_size = get_args().new_chunk_size
    max_mem = get_args().max_mem

    if not new_chunk_size or not tmp_zarr:
        print("new_chunk_size or tmp_zarr not provided, exiting.")
        return

    chunk_size = tuple(int(x) for x in new_chunk_size.split(','))

    rechunked = rechunk(source_group, target_chunks=chunk_size, target_store=output_zarr,
                        max_mem=max_mem,
                        temp_store=tmp_zarr)

    rechunked.execute()


setup(
    group="conversion",
    name="rechunk-zarr",
    version="0.1.0-SNAPSHOT",
    album_api_version="0.5.5",
    title="Rechunking ZARR files",
    description="This solution rechunks ZARR Files.",
    solution_creators=["Deborah Schmidt"],
    tags=["zarr"],
    license="MIT",
    args=[
        {
            "name": "input_zarr",
            "type": "directory",
            "description": "Path to input zarr.",
            "required": True
        },
        {
            "name": "output_zarr",
            "type": "directory",
            "description": "Rechunked ZARR directory. If not provided, the info of the input ZARR will be printed and nothing will be rechunked.",
        },
        {
            "name": "tmp_zarr",
            "type": "directory",
            "description": "Temporary ZARR directory.",
        },
        {
            "name": "new_chunk_size",
            "type": "string",
            "description": "New chunk size as comma-separated integers (e.g., 64,256,256).",
        },
        {
            "name": "max_mem",
            "type": "integer",
            "default": 25600000,
            "description": "Maximum memory used for chunking.",
        },
    ],
    run=run,
    dependencies={'environment_file': env_file}
)
