from album.runner.api import setup
import sys

env_file = """name:  napari_n2v 
channels:
    - conda-forge
    - defaults
dependencies:
    - python=3.9
    - scikit-image=0.22.0
    - pyqt=5.15.10
    - pip
    - pip:
        - tensorflow==2.15.0
        - napari==0.4.18
        - napari-n2v==0.1.1
"""

if sys.platform == "darwin":
    env_file = """name:  napari-n2v
channels:
  - conda-forge
  - defaults
  - nvidia
dependencies:
  - python=3.9
  - scikit-image=0.22.0
  - pyqt=5.15.10
  - imagecodecs
  - pip
  - pip:
        - tensorflow==2.15.0
        - napari==0.4.18
        - napari-n2v==0.1.1
"""


def run():
    import napari

    napari.Viewer()
    napari.run()


setup(
    group="denoise",
    name="launch-napari-n2v",
    version="0.1.0-SNAPSHOT",
    title="Launch napari-N2V",
    description="This solution launches napari with the n2v plugin.",
    solution_creators=["Ella Bahry"],
    cite=[{
        "text": "napari contributors (2019). napari: a multi-dimensional image viewer for python.",
        "doi": "10.5281/zenodo.3555620",
        "url": "https://github.com/napari/napari"
    },
    {
        "text": "N2V2 - Fixing Noise2Void Checkerboard Artifacts with Modified Sampling Strategies and a Tweaked Network Architecture",
        "url": "https://juglab.github.io/napari-n2v/"
    },],
    tags=["napari", "n2v"],
    license="MIT",
    album_api_version="0.5.5",
    run=run,
    dependencies={"environment_file": env_file},
)
