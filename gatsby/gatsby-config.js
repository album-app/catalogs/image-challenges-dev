module.exports = {
  pathPrefix: `/catalogs/image-challenges-dev`,
  siteMetadata: {
    title: 'Image Challenges Development Catalog',
    subtitle: 'Collecting solutions for common image tasks',
    catalog_url: 'https://gitlab.com/album-app/catalogs/image-challenges-dev',
    menuLinks:[
      {
         name:'Catalog',
         link:'/catalog'
      },
      {
         name:'About',
         link:'/about'
      },
    ]
  },
  plugins: [{ resolve: `gatsby-theme-album`, options: {} }],
}
